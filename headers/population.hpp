
#ifndef POPULATION_DEFINITION_HPP
#define POPULATION_DEFINITION_HPP
#include "structures.hpp"
#include "rt_graph.hpp"
#include <boost/thread/barrier.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <mutex>
#include <vector>
#include <random>
#include <fstream>
#include <atomic>

namespace sir
{
    /**
     * @brief class representing a population
     *
     */
    class society
    {
        /**
         * @brief current compute mode of this object
         * 
         */
        enum class compute_mode : char
        {
            epidemic = 'e',
            sample_gen = 's'
        };

        std::vector<individual> people_;       //vector containing all individuals
        std::vector<individual> people_tmp_;   //tmp vector containing all individuals, tmp data members are used to read the state in the last computed evolution
        std::mutex people_tmp_mutex_;          //this mutex is used to ensure a single coherent state of the population is drawn on screen
        std::vector<building> home_buildings_; //vector of buildings (homes)
        std::vector<building> work_buildings_; //vector of buildings (work places)

        //random numbers generator and distributions
        static std::mt19937_64 gen_;                    //mersenne twister pseudo-random number generator
        std::uniform_real_distribution<float> distr_;   //used to uniformly distribute positions
        std::uniform_real_distribution<float> distr_v_; //used to uniformly distribute velocities

        //output object
        std::ofstream output_file_;
        Graph2D_Ft realtime_graph_;
        Graph2D_Ft realtime_graph_aware_;

        //simulation parameters
        float const l_;                         //side length of the square in which the simulation takes place
        float const r_c_;                       //contagion radius surrounding an infected individual
        float const p_;                         //probability of contagion associated with 1 u.t. exposure to 1 infected individual
        float const P_rem_;                     //probability to be removed in a dt_
        float const d_rate_;
        float const dt_;                        //time length of evolution steps
        std::atomic<float> elapsed_time_ = 0.f; //elapsed time from the beginning
        float const min_inf_t_;                 //minimum duration of infection
        float const inc_t_;                     //time of incubation
        std::atomic<float> clock_ = 0.;         //current day time in u.t.
        float const day_length_ = 240.;         //day length in u.t.

        //counters
        std::mutex counters_mutex_;         //used to ensure coherence of displayed frames
        int population_size_;               //should be constant during simulation
        int S_;                             //susceptible individuals (during evolution)
        int I_ = 1;                         //infected individuals (during evolution)
        int R_ = 0;                         //removed individuals (during evolution)
        int D_ = 0;                         //deaths
        std::atomic_int n_ev_ = 0;          //total number of evolutions from the beginning
        std::atomic_int ev_ps_ = 0;         //upper limit of evolutions p.s.
        int evolutions_after_end_ = 0;      //counter of evolutions after end of epidemic
        day_phase phase_;                   //current day phase
        std::atomic_bool play_ = false;     //while true keeps simulating
        std::atomic_bool is_paused_ = true; //while true evolve method pauses
        std::atomic_bool behavior_ = true;  //decides whether individuals' behavior should be simulated

        //simulated counters
        int positive_tests_ = 0;
        int found_infections_ = 0;
        int estimated_active_cases_ = 0;
        int estimated_recovered_ = 0;
        int estimated_deceased_ = 0;
        int tested_ = 0;
        float const test_constant_ = 0;

        //windows data
        std::atomic_bool w1_ok_ = false; //when true window 1 (statistics) is currently rendering
        std::atomic_bool w2_ok_ = false; //when true window 2 (population) is currently rendering
        std::atomic_bool w3_ok_ = false; //when true window 3 (GUI) is currently rendering
        std::mutex windows_mutex_;
        std::mutex pause_mutex_;

        compute_mode const mode; //evolution mode of this society object
    public:
        society(epidemic_parameters const &par);
        society(int const Sample_Size, int const Contacts_per_Individual, float const Probability_of_Infection, float const DELTA_t);

        bool multi_evolve(epidemic_parameters const &par);
        bool single_evolve(epidemic_parameters const &par);
        bool generate_sample();
        bool stop();
        void display_windows(bool const windows, bool const display);

        int pop_size() const;
        int const &S() const;
        int const &I() const;
        int const &R() const;
        std::vector<building> const &home_buildings() const;
        std::vector<building> const &work_buildings() const;
        /**
         * @brief compare two societies based on their inizialization
         *
         * @param s1 first society object
         * @param s2 second society object
         * @return true s1 and s2 were initialized the same way
         * @return false s1 and s2 were initialized differently
         */
        friend bool operator==(society const &s1, society const &s2);
        /**
         * @brief compare two societies based on their inizialization
         *
         * @param s1 first society object
         * @param s2 second society object
         * @return true s1 and s2 were initialized differently
         * @return false s1 and s2 were initialized the same way
         */
        friend bool operator!=(society const &s1, society const &s2);

    private:
        //these methods are private to ensure only method "void evolve();" can modify private data members during simulation
        //definition in "evolution.cpp"

        void infect();
        void move();
        void accelerate();
        void compute_accelerations();
        void contacts();
        void remove();
        void hour_glass();
        void tmp();
        void update_counters();
        void deaths();

        //society behavior 

        void test_individuals();

        void multi_infect(boost::barrier &sync);
        void multi_move(boost::barrier &sync);
        void multi_accelerate(boost::barrier &sync);
        void multi_compute_accelerations(boost::barrier &sync);
        void multi_contacts(boost::barrier &sync);
        void multi_remove(boost::barrier &sync);
        void display_data(bool const windows);

        

        void draw_population_window(bool display);
        void draw_stat_window();
        void draw_GUI_window();
    };

} // namespace sir
#endif
