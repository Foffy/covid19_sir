
#ifndef DATA_STRUCTURES_HPP
#define DATA_STRUCTURES_HPP
#include <vector>
#include <string>
#include <iostream>
#include <string>
#include <cmath>
#include <fstream>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

namespace sir
{
    /**
     * @brief enumeration representing a day phase
     *
     */
    enum class day_phase : char
    {
        day = 'D',
        evening = 'E',
        night = 'N'
    };

    /**
     * @brief enumeration representing a S / I / R state
     *
     */
    enum class condition : char
    {
        S = 's',
        I = 'i',
        R = 'r',
        D = 'd'
    };

    /**
     * @brief enumeration representing a building's purpose
     *
     */
    enum class place : char
    {
        home = 'h',
        work = 'w',
    };

    /**
     * @brief a type of exception designed to be used by sir members
     *
     */
    class simulation_error
    {
        std::string err_msg = "Generic Error";
        bool const recoverable = false;

    public:
        simulation_error(bool const rec);

        /**
         * @brief Set whether the error should be recoverable or not
         * 
         * @param rec if true the error is considered recoverable
         */
        bool try_recover();

        /**
         * @brief sets the error message string
         *
         * @param err string to add to the error string
         */
        void what(std::string const &err);

        /**
         * @brief returns the error message
         *
         * @return std::string
         */
        std::string what() const;
    };

    /**
     * @brief class representing a 2-dimensional vector
     *
     */
    template <typename T>
    class Vector2D
    {
        //must be made of floating point numbers
        static_assert(std::is_floating_point_v<T>);
        T x_;
        T y_;

    public:
        Vector2D(
            T x = T{},
            T y = T{}) : x_{x}, y_{y} {}

        //used to modify vector's coordinates
        inline void operator()(T const &new_x, T const &new_y)
        {
            x_ = static_cast<T>(new_x);
            y_ = static_cast<T>(new_y);
        }

        //used to read vector's coordinates
        inline T x() const
        {
            return x_;
        }
        inline T y() const
        {
            return y_;
        }
    };

    //useful operators
    template <typename T>
    inline bool operator==(Vector2D<T> const &lhs, Vector2D<T> const &rhs)
    {
        return lhs.x() == rhs.x() && lhs.y() == rhs.y();
    }

    template <typename T>
    inline bool operator!=(Vector2D<T> const &lhs, Vector2D<T> const &rhs)
    {
        return lhs.x() != rhs.x() || lhs.y() != rhs.y();
    }

    template <typename T>
    inline Vector2D<T> operator+(Vector2D<T> const &lhs, Vector2D<T> const &rhs)
    {
        return Vector2D{lhs.x() + rhs.x(), lhs.y() + rhs.y()};
    }

    template <typename T>
    inline Vector2D<T> operator-(Vector2D<T> const &lhs, Vector2D<T> const &rhs)
    {
        return Vector2D{lhs.x() - rhs.x(), lhs.y() - rhs.y()};
    }

    template <typename T>
    inline void operator+=(Vector2D<T> &lhs, Vector2D<T> const &rhs)
    {
        lhs = Vector2D{lhs.x() + rhs.x(), lhs.y() + rhs.y()};
    }

    template <typename T, typename F>
    inline Vector2D<T> operator*(F const &factor, Vector2D<T> const &vector)
    {
        return Vector2D{static_cast<T>(factor) * vector.x(), static_cast<T>(factor) * vector.y()};
    }

    template <typename T, typename F>
    inline Vector2D<T> operator*(Vector2D<T> const &vector, F const &factor)
    {
        return Vector2D{static_cast<T>(factor) * vector.x(), static_cast<T>(factor) * vector.y()};
    }

    //useful functions
    template <typename T>
    inline T norm(Vector2D<T> const &v)
    {
        return hypot(v.x(), v.y());
    }

    template <typename T>
    inline T dot_product(Vector2D<T> const &lhs, Vector2D<T> const &rhs)
    {
        return lhs.x() * rhs.x() + lhs.y() * rhs.y();
    }

    /**
     * @brief structure containing an individual's health state
     *
     */
    struct SIR
    {
        bool tested_positive = false;
        bool has_symptoms = false;
        bool quarantined = false;
        int contacts = 0;
        long double T_I = 0.L;
        condition C;
    };

    /**
     * @brief structure containing information about a building
     *
     */
    struct building
    {
        Vector2D<float> position{0, 0};
        place P;
    };

    /**
     * @brief structure containing every information about an individual
     *
     */
    struct individual
    {
        Vector2D<float> position{0.F, 0.F};
        Vector2D<float> velocity{0.F, 0.F};
        Vector2D<float> acceleration{0.F, 0.F};
        SIR sir;
        int home_address = 0;
        bool has_work = false;
        int work_address;
    };

    /**
     * @brief contains information about a button's hitbox
     *
     */
    class button
    {
        sf::RectangleShape const &hitbox_;

    public:
        button(sf::RectangleShape const &hitbox) : hitbox_{hitbox} {}

        /**
         * @brief determines if button was clicked
         *
         * @param mouse vector representing mouse position
         * @return true - vector coordinates are inside the hitbox
         * @return false
         */
        bool is_pressed(sf::Vector2i const &mouse) const;
    };

    //derived from sim_parameters, specialized for epidemic mode
    struct epidemic_parameters
    {
        int N = 500;             //population size
        float l = 600.F;         //side length
        float p = 0.01F;         //probability of infection
        float dt = 0.1F;         //time step
        float r_c = 5.F;         //contagion radius
        float p_rem = 0.01F;     //probability of removal
        float d_rate = 0.10F;    //death rate
        float min_inf_t = 240.F; //time before an individual can be removed
        float inc_t = 120.F;     //incubation time
        bool win = false;        //if true stat and GUI windows are drawn
        bool display = false;    //if true population windows is drawn
        bool behavior = true;    //if true social behavior is enabled

        bool init_parameters();
        bool init_window() const;
        void configure();

    private:
       bool init_window_ = false;
        template <typename PAR>
        inline void set_parameter(std::string const &par_name, PAR &parameter)
        {
            std::fstream configuration("config.txt");
            float out;
            while (configuration.good())
            {
                std::string setting_name;
                configuration >> setting_name;
                if (setting_name == par_name)
                {
                    configuration >> out;
                    parameter = static_cast<PAR>(out);
                }
                else
                {
                    configuration >> setting_name;
                }
            }
        }
    };

} // namespace sir
#endif
