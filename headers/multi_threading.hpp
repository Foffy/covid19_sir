#ifndef MULTI_THREADING_HPP
#define MULTI_THREADING_HPP
#include <thread>
/**
     * @brief thread that automatically joins at the end of scope.
     *  NON COPYABLE and NON COPY-ASSIGNABLE
     */
class scoped_thread
{
    std::thread t;

public:
    /**
         * @brief Construct a new scoped thread object
         * 
         * @param t_ thread the object should own
         */
    explicit scoped_thread(std::thread t_) : t(std::move(t_))
    {
        if (!t.joinable())
            throw std::logic_error("No Thread");
    }
    ~scoped_thread()
    {
        t.join();
    }
    //delete copy constructor and copy assignment operator
    scoped_thread(scoped_thread const &) = delete;
    scoped_thread &operator=(scoped_thread const &) = delete;
};

/**
 * @brief ensures the associated thread is joined at the end of scope
 * 
 */
class thread_guard
{
    std::thread &t_;

public:
    /**
 * @brief Construct a new thread guard object
 * 
 * @param t thread to guard
 */
    explicit thread_guard(std::thread &t) : t_{t} {}
    ~thread_guard()
    {
        if (t_.joinable())
        {
            t_.join();
        }
    }
    thread_guard(thread_guard const&) = delete;
    thread_guard &operator=(thread_guard const &) = delete;
};
#endif