#include "population.hpp"

namespace sir
{
    std::mt19937_64 society::gen_(std::random_device{}());
    /**
         * @brief Construct a new society object, spawn individuals with random positions and velocities assign them to random buildings
         *
         * @param N number of individuals
         * @param l length of the square's side
         * @param p probability of infection
         * @param dt time step length
         * @param r_c minimum radius of contact
         * @param p_rem probability of being removed
         * @param min_inf_t minimum duration of infection
         */
    society::society(epidemic_parameters const &par) : people_(par.N),
                                                       distr_(0.0, 1.0),
                                                       distr_v_(-1.0, 1.0),
                                                       realtime_graph_{Vector2D<float>{0.f, 1.f},
                                                                       Vector2D<float>{0.f, 0.f /*float(par.N)*/},
                                                                       sf::Color::White},
                                                       realtime_graph_aware_{Vector2D<float>{0.f, 1.f},
                                                                             Vector2D<float>{0.f, 0.f /*float(par.N)*/},
                                                                             sf::Color::White},
                                                       l_{par.l}, r_c_{par.r_c},
                                                       p_{par.p}, P_rem_{1 - float(pow((1 - par.p_rem), par.dt))},
                                                       d_rate_{par.d_rate},
                                                       dt_{par.dt},
                                                       min_inf_t_{par.min_inf_t},
                                                       inc_t_{par.inc_t},
                                                       population_size_(par.N),
                                                       S_{par.N - 1},
                                                       behavior_(par.behavior),
                                                       test_constant_{1 + 0.00132f * population_size_},
                                                       mode(compute_mode::epidemic)
    {

        if (par.p_rem > 1 || par.p_rem < 0 || par.p > 1 || par.p < 0)
        {
            simulation_error e(true);
            e.what("Could not construct object - invalid probability ( T ʖ̯ T)");
            throw e;
        }

        if (par.N < 1)
        {
            simulation_error e(true);

            e.what("Could not construct object - there must be at least one individual ( T ʖ̯ T)");
            throw e;
        }
        if (par.r_c < 0)
        {
            simulation_error e(true);

            e.what("Could not construct object - invalid radius value ( T ʖ̯ T)");
            throw e;
        }
        if (l_ <= 0)
        {
            simulation_error e(true);

            e.what("Could not construct object - invalid side value ( T ʖ̯ T)");
            throw e;
        }
        if (par.dt <= 0 || par.min_inf_t < 0)
        {
            simulation_error e(true);

            e.what("Could not construct object - invalid time value ( T ʖ̯ T)");
            throw e;
        }

        std::uniform_real_distribution<float> distr_houses(0.2f, 0.8f);
        std::uniform_real_distribution<float> random_variable(0.f, 1.f);
        std::uniform_real_distribution<float> distr_workplaces(par.r_c / par.l, (par.l - par.r_c) / par.l);

        realtime_graph_.add_curve("S", sf::Color::White);
        realtime_graph_.add_curve("I", sf::Color::Red);
        realtime_graph_.add_curve("R", sf::Color::Green);
        realtime_graph_.add_curve("D", sf::Color::Magenta);
        realtime_graph_.add_curve("New Positives Found", sf::Color::Cyan);
        realtime_graph_.add_curve("Cumulative Infections", sf::Color::Yellow);

        realtime_graph_aware_.add_curve("Estimated Active Cases", sf::Color::Red);
        realtime_graph_aware_.add_curve("Estimated Recovered", sf::Color::Green);
        realtime_graph_aware_.add_curve("Estimated Deceased", sf::Color::Magenta);
        realtime_graph_aware_.add_curve("Number of Tests Today", sf::Color::White);


        if (par.behavior)
        {
            //discrete distribution used to determine the number of individuals per home
            std::discrete_distribution<int> distribution{0, 3, 4, 6, 5, 2, 1};
            int total_residents = 0;
            //assigning all individuals to homes
            for (int i = 0; total_residents != par.N; ++i)
            {
                building b;
                b.P = place::home;
                int residents = distribution(gen_);
                for (int n = 0; n != residents && total_residents != par.N; ++n)
                {
                    people_[total_residents].home_address = i;
                    ++total_residents;
                }
                home_buildings_.push_back(b);
            }
            //if there isn't enough space throw an exception
            if (pow((par.l * 3. / 5.), 2) / float(home_buildings_.size()) < pow((2. * r_c_), 2))
            {
                simulation_error e(true);

                e.what("Could not construct object - there is not enough space for houses! ( T ʖ̯ T)");
                throw e;
            }
            //initializing home buildings position
            for (int i = 0; i != int(home_buildings_.size()); ++i)
            {
                building &b = home_buildings_[i];
                b.position(distr_houses(gen_) * par.l, distr_houses(gen_) * par.l);
                if (i != 0)
                {
                    bool too_close = std::any_of(home_buildings_.begin(), home_buildings_.begin() + i, [&](building const &n) -> bool { return norm(b.position - n.position) < 2 * r_c_; });
                    for (int attempts_ = 0; too_close && attempts_ != par.N * par.N; ++attempts_)
                    {
                        b.position(distr_houses(gen_) * par.l, distr_houses(gen_) * par.l);
                        too_close = std::any_of(home_buildings_.begin(), home_buildings_.begin() + i, [&](building const &n) -> bool { return norm(b.position - n.position) < 2 * r_c_; });
                    }
                    if (too_close)
                    {
                        simulation_error e(true);

                        e.what("Could not construct object - there is not enough space for houses! ( T ʖ̯ T)");
                        throw e;
                    }
                }
            }

            int total_workers = 0;
            std::vector<int> indexes(par.N); //std::vector used to assign work places to individuals randomly
            std::iota(indexes.begin(), indexes.end(), 0);
            std::random_shuffle(indexes.begin(), indexes.end());
            for (int i = 0; total_workers < par.N * 3. / 4.; ++i)
            {
                building b;
                b.position(distr_workplaces(gen_) * par.l, distr_workplaces(gen_) * par.l);
                bool too_close = std::any_of(home_buildings_.begin(), home_buildings_.end(), [&](building const &n) -> bool { return norm(b.position - n.position) < 2 * r_c_; });
                for (int attempts_ = 0; too_close && attempts_ != par.N * par.N; ++attempts_)
                {
                    b.position(distr_workplaces(gen_) * par.l, distr_workplaces(gen_) * par.l);
                    too_close = std::any_of(home_buildings_.begin(), home_buildings_.end(), [&](building const &n) -> bool { return norm(b.position - n.position) < 2 * r_c_; });
                }
                if (too_close)
                {
                    simulation_error e(true);

                    e.what("Could not construct object - there is not enough space for houses! ( T ʖ̯ T)");
                    throw e;
                }
                b.P = place::work;
                int workers = distribution(gen_) * 2;
                for (int n = 0; n != workers && total_workers < par.N * 3. / 4.; ++n)
                {
                    people_[indexes[total_workers]].has_work = true;
                    people_[indexes[total_workers]].work_address = i;
                    ++total_workers;
                }
                work_buildings_.push_back(b);
            }

            //intializes individuals positions their home position
            for (individual &i : people_)
            {
                i.position = home_buildings_[i.home_address].position;
            }
        }
        else
        {
            for (individual &i : people_)
            {
                i.position(distr_(gen_) * l_, distr_(gen_) * l_);
            }
            home_buildings_.resize(0);
            work_buildings_.resize(0);
        }

        //intializes velocities randomly
        //intializes all indiduals to be susceptible
        for (individual &i : people_)
        {
            i.velocity(distr_v_(gen_) * par.r_c, distr_v_(gen_) * par.r_c);
            i.sir.C = condition::S;
            if (random_variable(gen_) < 0.01)
            {
                i.sir.has_symptoms = true;
            }
        }
        //initializes first individual to be infect
        people_[0].sir.C = condition::I;
        if (random_variable(gen_) < 0.8)
        {
            people_[0].sir.has_symptoms = true;
        }

        people_tmp_ = people_;
    }

    /**
     * @brief Construct a new society::society object
     * 
     * @param Sample_Size 
     * @param Contacts_per_Individual 
     * @param Probability_of_Infection 
     * @param DELTA_t 
     */
    society::society(int const Sample_Size, int const Contacts_per_Individual, float const Probability_of_Infection, float const DELTA_t)
        : l_(float((sqrt(Sample_Size)) * 2 + 2)), r_c_(1.0), p_(Probability_of_Infection), P_rem_{0L}, d_rate_{0.f}, dt_(DELTA_t), min_inf_t_(0.L), inc_t_(0.), mode(compute_mode::sample_gen)
    {
        int current_x = 1;
        int current_y = 1;
        individual default_ind;
        default_ind.has_work = false;
        default_ind.velocity(0.f, 0.f);
        default_ind.acceleration(0.f, 0.f);

        while (float(current_y) <= l_ - 1.f && int(people_.size()) != (Sample_Size + Sample_Size * Contacts_per_Individual))
        {
            individual current_individual = default_ind;
            while (float(current_x) <= l_ - 1.f && int(people_.size()) != (Sample_Size + Sample_Size * Contacts_per_Individual))
            {
                current_individual.position(current_x, current_y);
                current_individual.sir.C = condition::I;
                for (int infects = 0; infects != Contacts_per_Individual; ++infects)
                {
                    people_.push_back(current_individual);
                }
                current_individual.sir.C = condition::S;
                current_individual.sir.contacts = Contacts_per_Individual;
                people_.push_back(current_individual);

                current_x += 2;
            }
            current_x = 1;
            current_y += 2;
        }
        population_size_ = int(people_.size());
        people_tmp_ = people_;
        int S = 0;
        int I = 0;
        int R = 0;
        for (int i = 0; i != population_size_; ++i)
        {
            if (people_[i].sir.C == condition::S)
            {
                ++S;
            }
            else if (people_[i].sir.C == condition::I)
            {
                ++I;
            }
            else
            {
                ++R;
            }
        }
        S_ = S;
        I_ = I;
        R_ = R;
    }

} // namespace sir