#include "structures.hpp"
#include <algorithm>
#include <cassert>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <X11/Xlib.h>

namespace sir
{

    //simulation_error class definitions
    /**
     * @brief Construct a new simulation error::simulation error object
     * 
     * @param rec boolean value, if true exception is considered to be recoverable
     */
    simulation_error::simulation_error(bool const rec = false) : recoverable(rec) {}
    bool simulation_error::try_recover()
    {
        if (!recoverable)
        {
            std::cerr << "Could not recover.." << std::endl;
            return false;
        }
        else
        {
            if (!std::cin.good())
            {
                std::cin.clear();
                {
                    std::string ignoreLine;
                    std::getline(std::cin, ignoreLine);
                }
            }
            std::string t_again = "";
            std::cerr << "Would you like to try again?(yes/no)" << std::endl;
            std::cin >> t_again;
            while (t_again != "yes" && t_again != "no")
            {
                std::cerr << "Err. yes/no ?" << std::endl;
                std::cin >> t_again;
            }
            if (t_again == "no")
            {
                std::cerr << "Could not recover.." << std::endl;
                return false;
            }
            else
            {
                return true;
            }
        }
    }
    void simulation_error::what(std::string const &err)
    {
        err_msg = "Error: " + err;
    }
    std::string simulation_error::what() const
    {
        return err_msg;
    }

    //button class definitions

    bool button::is_pressed(sf::Vector2i const &mouse) const
    {
        if (mouse.x < hitbox_.getPosition().x + hitbox_.getSize().x && mouse.x > hitbox_.getPosition().x && mouse.y < hitbox_.getPosition().y + hitbox_.getSize().y && mouse.y > hitbox_.getPosition().y)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //sim_parameters struct definitions

    struct text_case
    {
    protected:
        sf::Text text_title;
        sf::Text content;
        sf::RectangleShape typing_area;
        button hitbox;

    public:
        /**
     * @brief Construct a new text case object
     * 
     * @param title title of text content
     * @param default_content default content string
     * @param font font used to draw characters
     * @param char_size size of characters
     * @param area area where text is typed
     */
        text_case(std::string const &title, std::string const &default_content, sf::Font const &font, int const char_size, sf::RectangleShape const &area) : typing_area(area), hitbox(typing_area)
        {
            text_title.setString(title);
            text_title.setFont(font);
            text_title.setCharacterSize(char_size);
            text_title.setPosition(sf::Vector2f(area.getPosition().x - sf::VideoMode::getDesktopMode().width / 3.5, area.getPosition().y));
            content.setFont(font);
            content.setString(default_content);
            content.setCharacterSize(char_size);
            content.setPosition(area.getPosition());
        }
        /**
         * @brief checks if the typing area was selected
         * 
         * @param mouse position vector of mouse
         * @return true - typing area was selected
         * @return false - typing area was not selected
         */
        bool is_pressed(sf::Vector2i const &mouse) const
        {
            return hitbox.is_pressed(mouse);
        }
        /**
         * @brief this function gets a unicode value and determines if a char should be added to the content string
         * 
         * @param unicode_char unicode value of character
         */
        virtual void enter_char(sf::Uint32 const &unicode_char) = 0;
        /**
         * @brief draw object on a sf::RenderWindow
         * 
         * @param win ref to window object
         */
        void draw(sf::RenderWindow &win) const
        {
            win.draw(typing_area);
            win.draw(content);
            win.draw(text_title);
        }
        /**
         * @brief Get number of characters in the content string
         * 
         * @return int - number of characters in the string
         */
        int get_contentstr_Size() const
        {
            return content.getString().getSize();
        }
        /**
         * @brief Get the contentstr object
         * 
         * @return std::string - content string
         */
        std::string get_contentstr() const
        {
            return content.getString().toAnsiString();
        }
        /**
         * @brief Get the content position object
         * 
         * @return sf::Vector2f - position of text content
         */
        sf::Vector2f get_content_position() const
        {
            return content.getPosition();
        }
        /**
         * @brief Get the area size object
         * 
         * @return sf::Vector2f - size of typing area
         */
        sf::Vector2f get_area_size() const
        {
            return typing_area.getSize();
        }
        /**
         * @brief Get the area position object
         * 
         * @return sf::Vector2f - get typing area position
         */
        sf::Vector2f get_area_position() const
        {
            return typing_area.getPosition();
        }
        /**
         * @brief Set the contentstr object
         * 
         * @param s string to set as content
         */
        void set_contentstr(std::string const &s)
        {
            content.setString(s);
        }
        /**
         * @brief Set the area color object
         * 
         * @param c fill color to set
         */
        void set_area_color(sf::Color const &c)
        {
            typing_area.setFillColor(c);
        }

    protected:
        /**
         * @brief add a char to the content string
         * 
         * @param unicode_char unicode value of character
         */
        void add_char(sf::Uint32 const &unicode_char)
        {
            content.setString(content.getString() + sf::String(unicode_char));
        }
    };
    /**
     * @brief this structure manages input for an unsigned value
     * 
     */
    struct unsigned_text : public text_case
    {
        //inheriting constructor
        using text_case::text_case;
        void enter_char(sf::Uint32 const &unicode_char) override
        {
            //Checking content string is not full
            if (content.getCharacterSize() * (content.getString().getSize()) < typing_area.getSize().x)
            {
                //if char is a number != 0 enter
                if (unicode_char > 48 && unicode_char < 58)
                {
                    add_char(unicode_char);
                }
                //else if char is 0 and string is not empty enter
                else if (unicode_char == 48 && !content.getString().isEmpty())
                {
                    add_char(unicode_char);
                }
            }
            //Manage backspace button, check string is not empty
            if (unicode_char == 8 && !content.getString().isEmpty())
            {
                std::string s = content.getString().toAnsiString();
                s.resize(content.getString().getSize() - 1);
                content.setString(s);
            }
        }
    };
    /**
     * @brief this structure manages input for a floating point
     * 
     */
    struct floating_point_text : public text_case
    {
        //inhereting constructor
        using text_case::text_case;

        void enter_char(sf::Uint32 const &unicode_char) override
        {
            //Checking content string is not full
            if (content.getCharacterSize() * (content.getString().getSize()) < typing_area.getSize().x)
            {
                //check string is not empty and '.' was typed
                if (unicode_char == 46 && !content.getString().isEmpty())
                {
                    //check there are no occurrences of '.'  then add '.'
                    int const index = content.getString().toAnsiString().find('.');
                    if (index == -1)
                    {
                        add_char(unicode_char);
                    }
                }
                //check typed character is '0'
                else if (unicode_char == 48)
                {
                    //check string is empty
                    if (content.getString().isEmpty())
                    {
                        add_char(unicode_char);
                    }
                    else
                    {
                        //check user doesn't type before '.' more than one '0' then add '0'
                        std::string const s = content.getString().toAnsiString();
                        int const occurrences = std::count(s.begin(), s.end(), '0');
                        if (occurrences != int(s.size()))
                        {
                            add_char(unicode_char);
                        }
                    }
                }
                //check a generic number was typed (except '0')
                else if (unicode_char > 48 && unicode_char < 58)
                {
                    //check that user doesn't type '0' and then a number at the start of the string, then add number
                    std::string const s = content.getString().toAnsiString();
                    int const occurrences = std::count(s.begin(), s.end(), '0');

                    if (occurrences != int(s.size()) || occurrences == 0)
                    {
                        add_char(unicode_char);
                    }
                }
            }
            //Manage backspace button, check string is not empty then delete last element
            if (!content.getString().isEmpty() && unicode_char == 8)
            {
                std::string s = content.getString().toAnsiString();
                s.resize(content.getString().getSize() - 1);
                content.setString(s);
            }
        }
    };
    /**
     * @brief this structure manages input for a floating point that has a max value
     * 
     */
    struct ranged_floating_point_text : public floating_point_text
    {
        //inhereting constructor
        using floating_point_text::floating_point_text;
        long double max = 0; //max value of floating point

        void enter_char(sf::Uint32 const &unicode_char) override
        {
            //Checking content string is not full
            if (content.getCharacterSize() * (content.getString().getSize()) < typing_area.getSize().x)
            {
                //check string is not empty and '.' was typed
                if (unicode_char == 46 && !content.getString().isEmpty())
                {
                    //check there are no occurrences of '.'  then add '.'
                    int const index = content.getString().toAnsiString().find('.');
                    if (index == -1)
                    {
                        add_char(unicode_char);
                    }
                }
                //check typed character is '0'
                else if (unicode_char == 48)
                {
                    std::string const s = content.getString().toAnsiString() + sf::String(unicode_char).toAnsiString();
                    long double value = std::stold(s.c_str());
                    //check string is empty
                    if (content.getString().isEmpty())
                    {
                        add_char(unicode_char);
                    }
                    //check value is in range
                    else if (value <= max)
                    {
                        std::string const s = content.getString().toAnsiString();
                        int const occurrences = std::count(s.begin(), s.end(), '0');
                        if (occurrences != int(s.size()))
                        {
                            add_char(unicode_char);
                        }
                    }
                }
                //check a generic number was typed (except '0')
                else if (unicode_char > 48 && unicode_char < 58)
                {
                    std::string const s1 = content.getString().toAnsiString() + sf::String(unicode_char).toAnsiString();
                    long double value = std::stold(s1.c_str());
                    std::string const s2 = content.getString().toAnsiString();
                    int const occurrences = std::count(s2.begin(), s2.end(), '0');
                    //check value is in range, check that user doesn't type '0' and then a number at the start of the string
                    if ((occurrences != int(s2.size()) || occurrences == 0) && (value <= max))
                    {
                        add_char(unicode_char);
                    }
                }
            }
            //Manage backspace button, check string is not empty then delete last element
            if (!content.getString().isEmpty() && unicode_char == 8)
            {
                std::string s = content.getString().toAnsiString();
                s.resize(content.getString().getSize() - 1);
                content.setString(s);
            }
        }
    };
    bool epidemic_parameters::init_window() const
    {
        return init_window_;
    }
    /**
     * @brief configure parameters initialization
     * 
     */
    void epidemic_parameters::configure()
    {
        std::fstream configuration("config.txt");

        while (configuration.good())
        {
            std::string text;
            configuration >> text;
            std::cout << text << "  ";
            configuration >> text;
            std::cout << text << '\n';
        }
        set_parameter("GRAPHICS_INTERFACE:", init_window_);
        set_parameter("POPULATION_SIZE:", N);
        set_parameter("TIME_STEP:", dt);
        set_parameter("SQUARE_SIDE:", l);
        set_parameter("INFECTION_PROBABILITY:", p);
        set_parameter("REMOVAL_PROBABILITY:", p_rem);
        set_parameter("DEATH_RATE:", d_rate);
        set_parameter("CONTAGION_RADIUS:", r_c);
        set_parameter("MINIMUM_INFECTION_TIME_DAYS:", min_inf_t);
        set_parameter("INCUBATION_TIME_DAYS:", inc_t);
    }
    /**
     * @brief create a window that inizializes epidemic simulation parameters
     * 
     */
    bool epidemic_parameters::init_parameters()
    {
        //Initializing concurrent access to Xlib
        XInitThreads();
        float h = sf::VideoMode::getDesktopMode().height;
        float w = sf::VideoMode::getDesktopMode().width;
        //this vector will point to text_case(s), it will be used to deduce where the user wants to type
        std::vector<text_case *> parameter_entries;

        sf::Font font;
        if (!font.loadFromFile("resources/Roboto_Mono/RobotoMono-VariableFont_wght.ttf"))
        {
            sir::simulation_error e(true);
            e.what("Could not load font");
            throw e;
        }
        //text_box is a rectangle representing the area where text will be typed or a button will be present
        sf::RectangleShape text_box;
        text_box.setFillColor(sf::Color(100, 100, 100));
        text_box.setSize(sf::Vector2f(1. / 6. * w, 1 / 20. * h));
        text_box.setOutlineColor(sf::Color::White);
        text_box.setOutlineThickness(text_box.getSize().y / 10.);
        text_box.setPosition(w / 3., h / 10.);
        int const charsize = int(1 / 40. * h);

        //initializing text_case(s) objects and pushing back pointers to "parameter_entries"
        //each parameter has a text_case

        unsigned_text individuals_text("Population Size: ", std::to_string(N), font, charsize, text_box);
        parameter_entries.push_back(&individuals_text);
        text_box.setPosition(w / 3., 2. * h / 10.);
        floating_point_text side_text("Side Length: ", std::to_string(l), font, charsize, text_box);
        parameter_entries.push_back(&side_text);
        text_box.setPosition(w / 3., 3. * h / 10.);
        ranged_floating_point_text p_cont_text("Probability of Contagion: ", std::to_string(p), font, charsize, text_box);
        p_cont_text.max = 1L;
        parameter_entries.push_back(&p_cont_text);
        text_box.setPosition(w / 3., 4. * h / 10.);
        ranged_floating_point_text dt_text("Time Step: ", std::to_string(dt), font, charsize, text_box);
        dt_text.max = 1L;
        parameter_entries.push_back(&dt_text);
        text_box.setPosition(w / 3., 5. * h / 10.);
        floating_point_text radius_text("Radius of Contagion: ", std::to_string(r_c), font, charsize, text_box);
        parameter_entries.push_back(&radius_text);
        text_box.setPosition(w / 3., 6. * h / 10.);
        ranged_floating_point_text p_rem_text("Probability of Removal: ", std::to_string(p_rem), font, charsize, text_box);
        p_rem_text.max = 1L;
        parameter_entries.push_back(&p_rem_text);
        text_box.setPosition(w / 3., 7. * h / 10.);
        floating_point_text t_min_inf_text("Minimum Time of Infection: ", std::to_string(min_inf_t), font, charsize, text_box);
        parameter_entries.push_back(&t_min_inf_text);
        text_box.setPosition(w * 4. / 5., h / 10.);
        floating_point_text t_inc_text("Incubation Period: ", std::to_string(inc_t), font, charsize, text_box);
        parameter_entries.push_back(&t_inc_text);

        text_box.setPosition(w / 3., 8. * h / 10.);
        (win) ? text_box.setFillColor(sf::Color::Green) : text_box.setFillColor(sf::Color::Red);

        text_box.setSize(sf::Vector2f(1. / 20. * h, 1 / 20. * h));
        unsigned_text win_toggle("Render Windows: ", "", font, charsize, text_box);
        text_box.setPosition(w / 3., 9. * h / 10.);

        if (win && display)
            text_box.setFillColor(sf::Color::Green);
        else
        {
            text_box.setFillColor(sf::Color::Red);
            display = false;
        }

        unsigned_text display_toggle("Render Population: ", "", font, charsize, text_box);
        text_box.setPosition(w * 4. / 5., 2. * h / 10);

        (behavior) ? text_box.setFillColor(sf::Color::Green) : text_box.setFillColor(sf::Color::Red);

        unsigned_text behavior_toggle("Simulate Behavior: ", "", font, charsize, text_box);
        text_box.setFillColor(sf::Color::Red);

        text_box.setPosition(w * 3. / 4., h * 3. / 4.);
        text_box.setSize(sf::Vector2f(1 / 3. * w, 1 / 6. * h));
        unsigned_text start_button("", "Start Simulation", font, charsize, text_box);
        text_box.setSize(sf::Vector2f(1 / 3. * w, 1 / 10. * h));
        text_box.setPosition(w * 3. / 4., h * 3. / 4. - text_box.getSize().y);
        unsigned_text abort_button("", "Abort", font, charsize, text_box);

        sf::Text max_dt_value; //max value of dt parameter
        max_dt_value.setFont(font);
        max_dt_value.setCharacterSize(charsize);
        max_dt_value.setString("(max = 1.0)");
        max_dt_value.setPosition(dt_text.get_area_position().x + dt_text.get_area_size().x + w / 60., dt_text.get_content_position().y);

        sf::RectangleShape current_textbox_indicator; //text cursor
        current_textbox_indicator.setFillColor(sf::Color(255, 255, 255, 80));
        current_textbox_indicator.setSize(sf::Vector2f(charsize / 2., 1.1 * charsize));

        sf::RenderWindow get_parameters(sf::VideoMode(w, h), "Simulation Initializer", sf::Style::Fullscreen);
        get_parameters.setVerticalSyncEnabled(true);
        bool default_par = false; //this var is used to determine if default values or user inserted values should be used
        bool can_continue = true; //determines if par. initialization exited successfully
        int c_t = 0;              //currently pointed text_case (none if c_t == 0)
        sf::Event e;              //event object used to poll window events
        //these clocks control the appearence of the text cursor
        sf::Clock time_since_last_appearance;
        sf::Clock appearence_length;
        //starting render loop
        while (get_parameters.isOpen())
        {

            individuals_text.draw(get_parameters);
            side_text.draw(get_parameters);
            p_cont_text.draw(get_parameters);
            dt_text.draw(get_parameters);
            radius_text.draw(get_parameters);
            p_rem_text.draw(get_parameters);
            t_min_inf_text.draw(get_parameters);
            t_inc_text.draw(get_parameters);
            win_toggle.draw(get_parameters);
            display_toggle.draw(get_parameters);
            behavior_toggle.draw(get_parameters);
            start_button.draw(get_parameters);
            abort_button.draw(get_parameters);

            get_parameters.draw(max_dt_value);

            //manage timed appearance of text cursor (text_case must be pointed)
            if (time_since_last_appearance.getElapsedTime().asMilliseconds() > 400 && c_t != 0)
            {
                if (appearence_length.getElapsedTime().asMilliseconds() < 800)
                {
                    float const &y = parameter_entries[c_t - 1]->get_area_position().y;
                    float const x = parameter_entries[c_t - 1]->get_area_position().x + parameter_entries[c_t - 1]->get_contentstr_Size() * charsize / 1.69;
                    current_textbox_indicator.setPosition(x, y);
                    get_parameters.draw(current_textbox_indicator);
                }
                else
                {
                    time_since_last_appearance.restart();
                    appearence_length.restart();
                }
            }

            get_parameters.display();
            get_parameters.clear(sf::Color::Black);

            //event loop polls events on current window
            while (get_parameters.pollEvent(e))
            {
                //checks if window was forced to close, if so load default parameters (default_par = true)
                if (e.type == sf::Event::Closed)
                {
                    std::cout << "Forcing window closure, default parameters will be used.." << '\n';
                    default_par = true;
                    get_parameters.close();
                }
                //manage left click
                else if (e.type == sf::Event::MouseButtonPressed && e.mouseButton.button == sf::Mouse::Left)
                {
                    sf::Vector2i const &localPosition = sf::Mouse::getPosition(); //ref to mouse position vector
                    //manage start button clicks
                    if (start_button.is_pressed(localPosition))
                    {
                        //when start is click check all parameters are initialized
                        bool complete = true;
                        for (int index = 0; index != int(parameter_entries.size()) && complete; ++index)
                        {
                            complete = !parameter_entries[index]->get_contentstr().empty();
                        }
                        if (complete)
                        {
                            //check dt value is != 0
                            if (std::stold(dt_text.get_contentstr().c_str()) == 0)
                            {
                                start_button.set_contentstr("Start Simulation \n Error: \n Time Step \n Must Be >0");
                            }
                            //check side length is != 0
                            else if (std::stold(side_text.get_contentstr().c_str()) == 0)
                            {
                                start_button.set_contentstr("Start Simulation \n Error: \n Side Length \n Must Be >0");
                            }
                            //all is ok -> close
                            else
                            {
                                get_parameters.close();
                            }
                        }
                        //unitialized parameters, do not exit and output error message
                        else
                        {
                            start_button.set_contentstr("Start Simulation \n Error: \n Uninitialized \n Parameters!");
                        }
                    }

                    //change current pointed text_case depending on clicked text case

                    else if (individuals_text.is_pressed(localPosition))
                    {
                        c_t = 1;
                    }
                    else if (side_text.is_pressed(localPosition))
                    {
                        c_t = 2;
                    }
                    else if (p_cont_text.is_pressed(localPosition))
                    {
                        c_t = 3;
                    }
                    else if (dt_text.is_pressed(localPosition))
                    {
                        c_t = 4;
                    }
                    else if (radius_text.is_pressed(localPosition))
                    {
                        c_t = 5;
                    }
                    else if (p_rem_text.is_pressed(localPosition))
                    {
                        c_t = 6;
                    }
                    else if (t_min_inf_text.is_pressed(localPosition))
                    {
                        c_t = 7;
                    }
                    else if (t_inc_text.is_pressed(localPosition))
                    {
                        c_t = 8;
                    }

                    //buttons managing bool parameters

                    else if (win_toggle.is_pressed(localPosition))
                    {
                        if (win)
                        {
                            win = false;
                            win_toggle.set_area_color(sf::Color::Red);
                            if (display)
                            {
                                display = false;
                                display_toggle.set_area_color(sf::Color::Red);
                            }
                        }
                        else
                        {
                            win = true;
                            win_toggle.set_area_color(sf::Color::Green);
                        }
                    }
                    else if (display_toggle.is_pressed(localPosition))
                    {
                        if (display)
                        {
                            display = false;
                            display_toggle.set_area_color(sf::Color::Red);
                        }
                        else if (win)
                        {
                            display = true;
                            display_toggle.set_area_color(sf::Color::Green);
                        }
                    }
                    else if (behavior_toggle.is_pressed(localPosition))
                    {
                        if (behavior)
                        {
                            behavior = false;
                            behavior_toggle.set_area_color(sf::Color::Red);
                        }
                        else
                        {
                            behavior = true;
                            behavior_toggle.set_area_color(sf::Color::Green);
                        }
                    }

                    //check if abort button was clicked

                    else if (abort_button.is_pressed(localPosition))
                    {
                        get_parameters.close();
                        can_continue = false;
                        default_par = true;
                    }

                    //if mouse left click hit nothing set c_t = 0 (no text_case is pointed)

                    else
                    {
                        c_t = 0;
                    }
                }
                //manage entered unicode text
                else if (e.type == sf::Event::TextEntered && c_t != 0)
                {
                    parameter_entries[c_t - 1]->enter_char(e.text.unicode); //enter typed characters (input is checked depending on object type)
                    //if tab was pressed go to next text case
                    if (e.text.unicode == 9 && c_t <= int(parameter_entries.size()))
                    {
                        //if the current text case is the last one go to the first one
                        if (c_t == int(parameter_entries.size()))
                        {
                            c_t = 1;
                        }
                        //else go to the next one
                        else
                        {
                            ++c_t;
                        }
                    }
                }
            }
        }
        //initialize parameters, default if default_par == true
        if (!default_par)
        {
            //get each text_case string value and initialize parameters

            int index = 0;
            std::string s = parameter_entries[index]->get_contentstr();
            N = std::stoi(s.c_str());

            s = parameter_entries[++index]->get_contentstr();
            l = std::stof(s.c_str());

            s = parameter_entries[++index]->get_contentstr();
            p = std::stof(s.c_str());

            s = parameter_entries[++index]->get_contentstr();
            dt = std::stof(s.c_str());

            s = parameter_entries[++index]->get_contentstr();
            r_c = std::stof(s.c_str());

            s = parameter_entries[++index]->get_contentstr();
            p_rem = std::stof(s.c_str());

            s = parameter_entries[++index]->get_contentstr();
            min_inf_t = std::stof(s.c_str());

            s = parameter_entries[++index]->get_contentstr();
            inc_t = std::stof(s.c_str());
        }
        return can_continue;
    }
} // namespace sir
