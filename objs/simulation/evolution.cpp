#include "population.hpp"
#include "multi_threading.hpp"
#include <thread>
#include <chrono>
#include <iostream>
#include <mutex>
using namespace sir;
/**
 * @brief this method computes the evolution of the epidemic by steps of length dt_
 * 
 * @param par parameters' object, influences how evolution is computed
 * @return true - evolution complete, there are no infected people left
 * @return false - evolution incomplete, there are still infected people left
 */
bool society::single_evolve(epidemic_parameters const &par)
{
    if (mode != compute_mode::epidemic)
    {
        sir::simulation_error error(false);
        error.what("Current object wasn't properly constructed to simulate an epidemic");
        throw error;
    }
    play_ = true;

    pause_mutex_.lock();
    scoped_thread windows_thread(std::thread([&par, this] { this->display_windows(par.win, par.display); }));

    if (par.win)
    {
        std::cout << "loading windows ..." << '\n';
    }

    //all data necessary to plot graphs is out put to Data.txt
    output_file_.open("Data.txt");
    output_file_ << std::fixed;
    std::cout << "Evolving ..." << '\n';

    while (play_)
    {
        if (is_paused_ && play_ && par.win)
        {
            pause_mutex_.lock();
            pause_mutex_.unlock();
        }

        if (ev_ps_ > 1000000)
        {
            ev_ps_ = 0;
        }
        if (ev_ps_ != 0)
        {
            std::this_thread::sleep_for(std::chrono::microseconds(unsigned(1000000. / ev_ps_)));
        }
        display_data(par.win);
        remove();
        contacts();
        infect();
        move();
        hour_glass();
        if (clock_ == 0)
        {
            test_individuals();
        }
        if (behavior_)
        {
            accelerate();
            compute_accelerations();
        }
        tmp();
        update_counters();
    }
    output_file_ << std::flush;
    output_file_.close();
    if (I_ == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}
/**
 * @brief computes individuals that will get infected in t += dt_
 *
 */
void society::infect()
{
    for (int i = 0; i != population_size_; ++i)
    {

        SIR const &tmp = people_tmp_[i].sir;

        if (tmp.C == condition::S)
        {
            //sect. 3.3 covid-19_SIR.pdf
            //p_ = p, tmp.contacts = c, dt_ = delta t
            //P = computed probability of infection for this delta_t
            float P = 1 - (pow((1 - p_), (tmp.contacts * dt_)));

            //distr_(gen_) is a random generated number from phi(x) = 1, x in [0,1]
            if (distr_(gen_) <= P)
            {
                //infect susceptible individual
                people_[i].sir.C = condition::I;
                if (distr_(gen_) <= 0.8)
                    people_[i].sir.has_symptoms = true;
            }
        }
    }
}

/**
 * @brief computes position of each individual in t += dt_
 *
 */
void society::move()
{
    for (int i = 0; i != population_size_; ++i)
    {
        if (!people_tmp_[i].sir.quarantined)
        {
            Vector2D<float> const &p = people_tmp_[i].position;
            Vector2D<float> const &v = people_tmp_[i].velocity;
            Vector2D<float> const &fut_p = people_tmp_[i].position + dt_ * people_tmp_[i].velocity;

            if (phase_ == day_phase::day &&
                !people_tmp_[i].has_work &&
                fut_p.x() > 0 &&
                fut_p.y() > 0 &&
                fut_p.x() < l_ &&
                fut_p.y() < l_)
            {
                for (int i_b = 0; i_b != int(work_buildings_.size()); ++i_b)
                {
                    Vector2D<float> const &b = work_buildings_[i_b].position;

                    if (norm(fut_p - b) < 2.0F * r_c_ + norm(v) * dt_)
                    {
                        Vector2D<float> const d{(p.y() - b.y()), (b.x() - p.x())};
                        float const refl_factor = (2.0F * (dot_product(v, d)) / dot_product(d, d));
                        people_[i].velocity = refl_factor * d - v;
                    }
                }
            }
            if (fut_p.x() > 0 &&
                fut_p.y() > 0 &&
                fut_p.x() < l_ &&
                fut_p.y() < l_)
            {
                people_[i].position += v * dt_;
            }
            else if ((fut_p.x() < 0 || fut_p.x() > l_) && (fut_p.y() > 0 || fut_p.y() < l_))
            {
                people_[i].velocity(-v.x(), v.y());
            }
            else if ((fut_p.y() < 0 || fut_p.y() > l_) && (fut_p.x() > 0 || fut_p.x() < l_))
            {
                people_[i].velocity(v.x(), -v.y());
            }
            else
            {
                people_[i].velocity(-v.x(), -v.y());
            }
        }
    }
}
/**
 * @brief applies accelerations computed by compute_accelerations() method
 *
 */
void society::accelerate()
{
    if (clock_ == 0)
    {
        for (int i = 0; i != population_size_; ++i)
        {
            if (!people_[i].sir.quarantined)
            {
                people_[i].velocity(distr_v_(gen_) * r_c_, distr_v_(gen_) * r_c_);
            }
        }
    }
    else if (phase_ == day_phase::evening && clock_ < day_length_ * 1. / 3. + dt_)
    {
        for (int i = 0; i != population_size_; ++i)
        {
            if (people_tmp_[i].has_work && !people_[i].sir.quarantined)
            {
                people_[i].velocity(distr_v_(gen_) * r_c_, distr_v_(gen_) * r_c_);
            }
        }
    }
    for (int i = 0; i != population_size_; ++i)
    {
        if (!people_[i].sir.quarantined)
        {
            people_[i].velocity += people_tmp_[i].acceleration * dt_;
        }
    }
}
/**
 * @brief manages behaviour of individuals
 *
 */
void society::compute_accelerations()
{
    switch (phase_)
    {

    case day_phase::day:
        for (int i = 0; i != population_size_; ++i)
        {
            if (people_tmp_[i].has_work)
            {
                Vector2D<float> const &p = people_tmp_[i].position;
                building const &b = work_buildings_[people_tmp_[i].work_address];

                people_[i].acceleration = 0.1 * (b.position - p) - 0.5 * people_tmp_[i].velocity;
            }
            else
            {
                people_[i].acceleration(0, 0);
            }
        }
        break;
    case day_phase::evening:
        for (int i = 0; i != population_size_; ++i)
        {
            people_[i].acceleration(0, 0);
        }
        break;
    case day_phase::night:
        for (int i = 0; i != population_size_; ++i)
        {
            Vector2D<float> const &p = people_tmp_[i].position;
            building const &b = home_buildings_[people_[i].home_address];

            people_[i].acceleration = 0.1 * (b.position - p) - 0.5 * people_tmp_[i].velocity;
        }
        break;
    }
}

/**
 * @brief computes c(t + dt_) for every indidividual
 *
 */
void society::contacts()
{
    for (int i = 0; i != population_size_; ++i)
    {
        if (people_tmp_[i].sir.C == condition::S)
        {
            people_[i].sir.contacts = std::count_if(people_tmp_.begin(), people_tmp_.end(), [&](individual const &n) -> bool { return (n.sir.C == condition::I && n.sir.T_I > inc_t_ && norm(people_tmp_[i].position - n.position) < r_c_); });
        }
    }
}
/**
 * @brief computes individuals that will be removed in t += dt_
 *
 */
void society::remove()
{
    for (int i = 0; i != population_size_; ++i)
    {

        if (people_tmp_[i].sir.C == condition::I)
        {

            people_[i].sir.T_I += dt_;

            SIR const &tmp = people_tmp_[i].sir;

            if (tmp.T_I > min_inf_t_)
            {
                //sect. 4 covid-19_SIR.pdf
                //distr_(gen_) is a random number from phi(x) = 1, x in [0,1]
                //P_rem_ = delta P_rem, it is constant
                if (distr_(gen_) <= P_rem_)
                {
                    if (distr_(gen_) <= d_rate_)
                    {
                        people_[i].sir.C = condition::D;
                    }
                    else
                    {
                        //remove infect individual
                        people_[i].sir.C = condition::R;
                    }
                }
            }
        }
    }
}
/**
 * @brief computes time and phase of current day
 *
 */
void society::hour_glass()
{
    if (clock_ + dt_ < day_length_)
    {
        clock_ = clock_ + dt_;
    }
    else
    {
        clock_ = 0L;
    }

    if (clock_ >= 0L && clock_ < day_length_ * 1. / 3.)
    {
        phase_ = day_phase::day;
    }

    else if (clock_ >= day_length_ * 1. / 3. && clock_ < day_length_ * 2. / 3.)
    {
        phase_ = day_phase::evening;
    }

    else
    {
        phase_ = day_phase::night;
    }
}
/**
 * @brief updates tmp private data members
 *
 */
void society::tmp()
{
    people_tmp_mutex_.lock();
    people_tmp_ = people_;
    people_tmp_mutex_.unlock();
}
void society::update_counters()
{
    counters_mutex_.lock();
    S_ = 0;
    I_ = 0;
    R_ = 0;
    D_ = 0;
    for (auto const &c : people_)
    {
        switch (c.sir.C)
        {
        case condition::S:
            ++S_;
            break;
        case condition::I:
            ++I_;
            break;
        case condition::R:
            ++R_;
            break;
        case condition::D:
            ++D_;
            break;
        }
    }
    elapsed_time_ = n_ev_ * dt_;
    ++n_ev_;
    if ((estimated_active_cases_ != 0 || I_ != 0) && clock_ == 0)
    {
        //realtime_graph.add_point("S", Vector2D<float>{elapsed_time_ / day_length_, float(S_)});
        realtime_graph_.add_point("I", Vector2D<float>{elapsed_time_ / day_length_, float(I_)});
        realtime_graph_.add_point("R", Vector2D<float>{elapsed_time_ / day_length_, float(R_)});
        realtime_graph_.add_point("D", Vector2D<float>{elapsed_time_ / day_length_, float(D_)});
        realtime_graph_.add_point("New Positives Found", Vector2D<float>{elapsed_time_ / day_length_, float(positive_tests_)});
        realtime_graph_.add_point("Cumulative Infections", Vector2D<float>{elapsed_time_ / day_length_, float(R_ + I_ + D_)});

        realtime_graph_aware_.add_point("Estimated Active Cases", Vector2D<float>{elapsed_time_ / day_length_, float(estimated_active_cases_)});
        realtime_graph_aware_.add_point("Estimated Recovered", Vector2D<float>{elapsed_time_ / day_length_, float(estimated_recovered_)});
        realtime_graph_aware_.add_point("Estimated Deceased", Vector2D<float>{elapsed_time_ / day_length_, float(estimated_deceased_)});
        realtime_graph_aware_.add_point("Number of Tests Today", Vector2D<float>{elapsed_time_ / day_length_, float(tested_)});
        tested_ = 0;
    }
    counters_mutex_.unlock();
}
/**
 * @brief writes graph data to file
 *
 * @param windows if windows is false windows are not rendered so the program will close when I_ == 0
 */
void society::display_data(bool const windows)
{
    if (I_ != 0)
    {
        output_file_ << elapsed_time_ << "    " << S_ << "    " << I_ << "    " << R_ << "   " << D_ << "  " << S_ + I_ + R_ << "    " << population_size_ - S_ << '\n';
    }
    else if (evolutions_after_end_ == 0)
    {
        output_file_ << elapsed_time_ << "    " << S_ << "    " << I_ << "    " << R_ << "    " << D_ << "  " << S_ + I_ + R_ << "    " << population_size_ - S_ << '\n';
        ++evolutions_after_end_;
    }
    else if (!windows)
    {
        stop();
    }
}
/**
 * @brief stops evolution and closes windows
 *
 * @return true if evolution was running
 * @return false if evolution was already stopped
 */
bool society::stop()
{
    if (play_)
    {
        play_ = false;
        return true;
    }
    else
    {
        return false;
    }
}