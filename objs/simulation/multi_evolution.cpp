#include "population.hpp"
#include <thread>
#include <chrono>
#include <iostream>
#include <boost/thread.hpp>
#include <boost/thread/barrier.hpp>

namespace sir
{

    bool society::multi_evolve(epidemic_parameters const &par)
    {
        if (mode != compute_mode::epidemic)
        {
            sir::simulation_error error(false);
            error.what("Current object wasn't properly constructed to simulate an epidemic");
            throw error;
        }

        play_ = true;
        pause_mutex_.lock();
        std::thread windows_thread([&par, this] { this->display_windows(par.win, par.display); });

        if (par.win)
        {
            std::cout << "loading windows ..." << '\n';
        }

        boost::barrier sync_bar(3);
        //boost::thread remove([this, &sync_bar] { this->multi_remove(sync_bar); });
        boost::thread move([this, &sync_bar] { this->multi_move(sync_bar); });
        //boost::thread infect([this, &sync_bar] { this->multi_infect(sync_bar); });
        boost::thread contact([this, &sync_bar] { this->multi_contacts(sync_bar); });
        //boost::thread acc([this, &sync_bar] { this->multi_accelerate(sync_bar); });
        //boost::thread c_acc([this, &sync_bar] { this->multi_compute_accelerations(sync_bar); });

        //all data necessary to plot graphs is out put to Data.txt
        output_file_.open("Data.txt");
        output_file_ << std::fixed;
        std::cout << "Evolving ..." << '\n';
        while (play_)
        {
            sync_bar.wait();
            if (ev_ps_ > 1000000)
            {
                ev_ps_ = 0;
            }
            if (ev_ps_ != 0)
            {
                std::this_thread::sleep_for(std::chrono::microseconds(unsigned(1000000. / ev_ps_)));
            }
            if (is_paused_ && play_ && par.win)
            {
                pause_mutex_.lock();
                pause_mutex_.unlock();
            }
            remove();
            infect();
            if (behavior_)
            {
                accelerate();
                compute_accelerations();
            }
            sync_bar.wait();
            if (behavior_)
            {
                hour_glass();
            }
            update_counters();
            display_data(par.win);
            tmp();
        }
        output_file_ << std::flush;
        output_file_.close();
        //infect.interrupt();
        //remove.interrupt();
        contact.interrupt();
        //acc.interrupt();
        //c_acc.interrupt();
        move.interrupt();

        move.join();
        //infect.join();
        //remove.join();
        contact.join();
        //acc.join();
        //c_acc.join();
        windows_thread.join();
        if (I_ == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    void society::multi_infect(boost::barrier &sync)
    {
        while (true)
        {
            sync.wait();
            infect();
            sync.wait();
        }
    }
    void society::multi_move(boost::barrier &sync)
    {
        while (true)
        {
            sync.wait();
            move();
            sync.wait();
        }
    }
    void society::multi_accelerate(boost::barrier &sync)
    {
        while (true)
        {
            sync.wait();
            if (behavior_)
            {
                accelerate();
            }
            sync.wait();
        }
    }

    void society::multi_compute_accelerations(boost::barrier &sync)
    {
        while (true)
        {
            sync.wait();
            if (behavior_)
            {
                compute_accelerations();
            }
            sync.wait();
        }
    }
    void society::multi_contacts(boost::barrier &sync)
    {
        while (true)
        {
            sync.wait();
            contacts();
            sync.wait();
        }
    }

    void society::multi_remove(boost::barrier &sync)
    {
        while (true)
        {
            sync.wait();
            remove();
            sync.wait();
        }
    }
} // namespace sir
