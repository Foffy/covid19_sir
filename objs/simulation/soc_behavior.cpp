#include "population.hpp"

void sir::society::test_individuals()
{
    std::uniform_int_distribution index_distr(0, population_size_ - 1);
    int const number_of_tests = int(3.59 * positive_tests_ + 0.005 * population_size_ + estimated_active_cases_ +  64 * estimated_deceased_ + 1);
    estimated_deceased_ = 0;
    int new_pos = 0;
    int n = 0;
    for (int tested = 0; tested != number_of_tests;)
    {
        individual &guinea_pig = people_[index_distr(gen_)];
        if (guinea_pig.sir.C != condition::D)
        {
            if (guinea_pig.sir.has_symptoms)
            {
                if (guinea_pig.sir.C == condition::I && !guinea_pig.sir.tested_positive && guinea_pig.sir.T_I > inc_t_)
                {
                    ++new_pos;
                    ++found_infections_;
                    guinea_pig.sir.tested_positive = true;
                    guinea_pig.sir.quarantined = true;
                    guinea_pig.velocity = Vector2D<float>{0.f,0.f};
                    guinea_pig.acceleration = Vector2D<float>{0.f,0.f};
                    guinea_pig.position = Vector2D<float>{-10.f,10.f};
                }
                else if (guinea_pig.sir.C == condition::R && guinea_pig.sir.tested_positive)
                {
                    ++estimated_recovered_;
                    guinea_pig.sir.tested_positive = false;
                }

                ++tested;
                ++tested_;
            }
        }
        else
        {
            if(guinea_pig.sir.tested_positive)
            {
                ++estimated_deceased_;
                guinea_pig.sir.tested_positive = false;
            }
        }
        estimated_active_cases_ = found_infections_ - estimated_recovered_ - estimated_deceased_;
        ++n;
        if (n == 10 * population_size_)
            break;
    }
    positive_tests_ = new_pos;
}