
#include "population.hpp"
#include "rt_graph.hpp"
#include "multi_threading.hpp"
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <thread>
#include <vector>
#include <mutex>
#include <X11/Xlib.h>

namespace sir
{
    /**
     * @brief initializes parallel rendering of windows
     *
     * @param windows if false does nothing
     * @param display if true displays a representation of the population
     */
    void society::display_windows(bool const windows, bool const display)
    {
        if (windows)
        {
            //initializing concurrent access to Xlib
            XInitThreads();
            //initializing threads with window drawing methods
            scoped_thread stat_w_thread(std::thread([this] { this->draw_stat_window(); }));
            scoped_thread gui_w_thread(std::thread([this] { this->draw_GUI_window(); }));
            scoped_thread pop_w_thread(std::thread([this, display] { this->draw_population_window(display); }));
        }
    }

    /**
     * @brief initializes and renders statistics window
     * 
     */
    void society::draw_stat_window()
    {
        if (mode == compute_mode::epidemic)
        {
            windows_mutex_.lock();
            float h = sf::VideoMode::getDesktopMode().height;
            float w = sf::VideoMode::getDesktopMode().width;
            float D; //scaling factor
            if (w < h)
                D = l_ / (w * 9 / 10);
            else
                D = l_ / (h * 9 / 10);

            sf::Vector2i s_w_pos(l_ / D, 0);

            sf::ContextSettings settings;
            settings.antialiasingLevel = 0;

            sf::RenderWindow stat_window(sf::VideoMode(w - (l_ / D), h / 2), "Graph", sf::Style::Default, settings);
            stat_window.setPosition(s_w_pos);
            sf::View stat_view(sf::FloatRect(0, 0, w - (l_ / D), h / 2));
            sf::Event e;
            sf::Clock nav_clock;

            realtime_graph_.set_y_title("Individuals");
            realtime_graph_.set_x_title("Time (Days)");

            realtime_graph_aware_.set_y_title("Individuals");
            realtime_graph_aware_.set_x_title("Time (Days)");

            windows_mutex_.unlock();
            while (stat_window.isOpen())
            {
                while (stat_window.pollEvent(e))
                {
                    //resizing view when mousewheel is scrolled
                    if (e.type == sf::Event::MouseWheelScrolled)
                    {
                        if (e.mouseWheelScroll.delta < 0 && (stat_view.getSize().x * (1 - e.mouseWheelScroll.delta / 10.) <= 2. * l_ / D))
                        {
                            stat_view.setSize(stat_view.getSize().x * (1 - e.mouseWheelScroll.delta / 10.), stat_view.getSize().y * (1 - e.mouseWheelScroll.delta / 10.));
                        }
                        else if (e.mouseWheelScroll.delta > 0 && (stat_view.getSize().x / (1 + e.mouseWheelScroll.delta / 10.) >= 10))
                        {
                            stat_view.setSize(stat_view.getSize().x / (1 + e.mouseWheelScroll.delta / 10.), stat_view.getSize().y / (1 + e.mouseWheelScroll.delta / 10.));
                        }
                    }
                    //if window is resized change view
                    else if (e.type == sf::Event::Resized)
                    {
                        // update the view to the new size of the window
                        stat_view.setSize(e.size.width, e.size.height);
                    }
                    else if (e.type == sf::Event::Closed)
                    {
                        stat_window.close();
                    }
                }
                if (!play_)
                {
                    stat_window.close();
                }
                if (stat_window.hasFocus() && (nav_clock.getElapsedTime().asMilliseconds() > 5))
                {
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && ((stat_view.getCenter().y - (stat_view.getSize().y / 2.) - stat_view.getSize().y / 200.) >= -0.5 * realtime_graph_.get_Size().y() / D + 250.f))
                    {
                        stat_view.move(sf::Vector2f(0, -stat_view.getSize().y / 200.));
                    }
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && ((stat_view.getCenter().y + (stat_view.getSize().y / 2.) + stat_view.getSize().y / 200.) <= 1.5 * realtime_graph_.get_Size().y() / D + 250.f))
                    {
                        stat_view.move(sf::Vector2f(0, stat_view.getSize().y / 200.));
                    }
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && ((stat_view.getCenter().x - (stat_view.getSize().x / 2.) - stat_view.getSize().x / 200.) >= -0.5 * realtime_graph_.get_Size().x() / D + 250.f))
                    {
                        stat_view.move(sf::Vector2f(-stat_view.getSize().x / 200., 0));
                    }
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && ((stat_view.getCenter().x + (stat_view.getSize().x / 2.) + stat_view.getSize().x / 200.) <= 1.5 * realtime_graph_.get_Size().x() / D + 250.f))
                    {
                        stat_view.move(sf::Vector2f(stat_view.getSize().x / 200., 0));
                    }
                    nav_clock.restart();
                }
                if (realtime_graph_.get_drawn_points() != realtime_graph_.get_total_points() || realtime_graph_.get_total_points() == 0)
                {
                    stat_window.clear();
                    counters_mutex_.lock();
                    realtime_graph_.draw_graph(stat_window, Vector2D<float>{stat_window.getSize().x / 4.f, stat_window.getSize().y / 4.f}, Vector2D{500.f, 250.f});
                    realtime_graph_.draw_leg(stat_window);
                    realtime_graph_aware_.draw_graph(stat_window, Vector2D<float>{stat_window.getSize().x / 4.f, stat_window.getSize().y / 4.f + 313.f}, Vector2D{500.f, 250.f});
                    realtime_graph_aware_.draw_leg(stat_window);
                    counters_mutex_.unlock();
                    stat_window.display();
                }
                else
                {
                    stat_window.display();
                }

                stat_window.setView(stat_view);

                w1_ok_ = true;
            }
            w2_ok_ = false;
        }
    }
    /**
     * @brief initializes and renders graphic user interface window
     * 
     */
    void society::draw_GUI_window()
    {
        windows_mutex_.lock();
        float h = sf::VideoMode::getDesktopMode().height;
        float w = sf::VideoMode::getDesktopMode().width;
        float D;
        if (w < h)
            D = l_ / (w * 9 / 10);
        else
            D = l_ / (h * 9 / 10);

        sf::Texture bg_texture;
        sf::Texture pp;
        sf::Texture su;
        sf::Texture sd;
        sf::Texture rt;
        sf::Texture hit;
        sf::Texture start;
        //loading gui textures from resources
        if (!((bg_texture.loadFromFile("resources/icon.png")) &&
              (pp.loadFromFile("resources/play_pause.png")) &&
              (su.loadFromFile("resources/2X.png")) &&
              (sd.loadFromFile("resources/05X.png")) &&
              (rt.loadFromFile("resources/realtime.png")) &&
              (hit.loadFromFile("resources/hitbox.png")) &&
              (start.loadFromFile("resources/population_window.png"))))
        {
            sir::simulation_error e(true);
            e.what("Could not load resources");
            throw e;
        }
        //smoothing textures
        pp.setSmooth(true);
        su.setSmooth(true);
        sd.setSmooth(true);
        rt.setSmooth(true);
        start.setSmooth(true);
        //background sprite
        sf::Sprite virus_icon;
        virus_icon.setTexture(bg_texture);
        //creating window
        sf::Vector2i w_pos(l_ / D, h / 2.f + h / 10.f);

        sf::RenderWindow gui_window(sf::VideoMode(w - (l_ / D), h / 3), "Simulation Control Panel", sf::Style::Close);
        sf::Vector2u size = gui_window.getSize();
        gui_window.setActive(true);
        gui_window.setPosition(w_pos);
        gui_window.setVerticalSyncEnabled(true);

        //setting window icon
        sf::Image icon;
        if (icon.loadFromFile("resources/w_icon.png"))
            gui_window.setIcon(128, 128, icon.getPixelsPtr());
        else
            std::cerr << "Could not load windows icon" << std::endl;

        //setting background image scale
        virus_icon.setScale((w - (l_ / D)) / (3 * 1024), (w - (l_ / D)) / (3 * 1024));
        virus_icon.setPosition(0, gui_window.getSize().y - 1028 * virus_icon.getScale().y);

        //pause button
        sf::RectangleShape pause_button;
        pause_button.setFillColor(sf::Color(100, 100, 100));
        pause_button.setSize(sf::Vector2f(size.x / 10, size.x / 10));
        pause_button.setPosition(sf::Vector2f(0, 0));
        pause_button.setTexture(&pp, false);
        sir::button p_b(pause_button);
        //speed-up button
        sf::RectangleShape speedup_button;
        speedup_button.setFillColor(sf::Color(100, 100, 100));
        speedup_button.setSize(sf::Vector2f(size.x / 10, size.x / 10));
        speedup_button.setPosition(sf::Vector2f(pause_button.getSize().x, 0));
        speedup_button.setTexture(&su, false);
        sir::button su_b(speedup_button);
        //slow-down button
        sf::RectangleShape slowdown_button;
        slowdown_button.setFillColor(sf::Color(100, 100, 100));
        slowdown_button.setSize(sf::Vector2f(1.56 * size.x / 10, size.x / 10));
        slowdown_button.setPosition(sf::Vector2f(pause_button.getSize().x + speedup_button.getSize().x, 0));
        slowdown_button.setTexture(&sd, false);
        sir::button sd_b(slowdown_button);
        //real-time button
        sf::RectangleShape realtime_button;
        realtime_button.setFillColor(sf::Color(100, 100, 100));
        realtime_button.setSize(sf::Vector2f(size.x / 10, size.x / 10));
        realtime_button.setPosition(sf::Vector2f(pause_button.getSize().x + speedup_button.getSize().x + slowdown_button.getSize().x, 0));
        realtime_button.setTexture(&rt, false);
        sir::button rt_b(realtime_button);
        //start population window button
        sf::RectangleShape start_window_button;
        start_window_button.setFillColor(sf::Color(100, 100, 100));
        start_window_button.setSize(sf::Vector2f(size.x / 5., 0.77 * size.x / 5.));
        start_window_button.setTexture(&start, false);
        button sw_b(start_window_button);

        //active button box
        sf::RectangleShape active_box;
        active_box.setTexture(&hit);
        active_box.setSize(sf::Vector2f(size.x / 10, size.y / 10));
        //sfml event
        sf::Event e;
        std::thread new_window;
        thread_guard window_guard(new_window);
        //STATISTICS
        //loading font
        sf::Font data_font;
        if (!data_font.loadFromFile("resources/Roboto_Mono/RobotoMono-VariableFont_wght.ttf"))
        {
            sir::simulation_error e(true);
            e.what("Could not load font");
            throw e;
        }
        //creating text objects
        sf::Text S;
        S.setFont(data_font);
        sf::Text I;
        I.setFont(data_font);
        sf::Text R;
        R.setFont(data_font);
        sf::Text c_D;
        c_D.setFont(data_font); //deaths counter
        sf::Text t_e;
        t_e.setFont(data_font);
        sf::Text evo;
        evo.setFont(data_font);

        I.setFillColor(sf::Color::Red);
        R.setFillColor(sf::Color::Green);
        c_D.setFillColor(sf::Color::Magenta);
        t_e.setFillColor(sf::Color(100, 100, 100));
        evo.setFillColor(sf::Color(100, 100, 100));

        float text_scaling = (w - (l_ / D)) / 50; //text scaling factor

        S.setCharacterSize(text_scaling);
        I.setCharacterSize(text_scaling);
        R.setCharacterSize(text_scaling);
        c_D.setCharacterSize(text_scaling);
        t_e.setCharacterSize(text_scaling);
        evo.setCharacterSize(text_scaling);

        S.setPosition(gui_window.getSize().x / 2.f, 0);
        I.setPosition(gui_window.getSize().x / 2.f, (text_scaling + text_scaling / 10));
        R.setPosition(gui_window.getSize().x / 2.f, 2 * (text_scaling + text_scaling / 10));
        c_D.setPosition(gui_window.getSize().x / 2.f, 3 * (text_scaling + text_scaling / 10));
        t_e.setPosition(gui_window.getSize().x / 2.f, 4 * (text_scaling + text_scaling / 10));
        evo.setPosition(gui_window.getSize().x / 2.f, 5 * (text_scaling + text_scaling / 10));

        sf::RectangleShape stat_background;
        stat_background.setFillColor(sf::Color::Black);
        stat_background.setSize(sf::Vector2f(gui_window.getSize().x / 2.f, 7 * (text_scaling + text_scaling / 10)));
        stat_background.setPosition(gui_window.getSize().x / 2.f, 0);
        start_window_button.setPosition(sf::Vector2f(stat_background.getPosition().x, stat_background.getSize().y));

        windows_mutex_.unlock();

        while (gui_window.isOpen())
        {
            gui_window.clear(sf::Color::White);
            gui_window.draw(virus_icon);
            //checking all events
            while (gui_window.pollEvent(e))
            {
                //managing left click
                if (e.type == sf::Event::MouseButtonPressed && e.mouseButton.button == sf::Mouse::Left)
                {
                    //gets a vector with the mouse position
                    sf::Vector2i const &localPosition = sf::Mouse::getPosition(gui_window);

                    //checks if buttons hitboxes were hit

                    if (p_b.is_pressed(localPosition))
                    {
                        if (is_paused_)
                        {
                            is_paused_ = false;
                            pause_mutex_.unlock();
                        }
                        else
                        {
                            is_paused_ = true;
                            pause_mutex_.lock();
                        }
                    }
                    else if (su_b.is_pressed(localPosition))
                    {
                        ev_ps_ += ev_ps_;
                        active_box.setPosition(speedup_button.getPosition());
                        active_box.setSize(speedup_button.getSize());
                        gui_window.draw(active_box);
                    }
                    else if (sd_b.is_pressed(localPosition))
                    {
                        if (ev_ps_ / 2 < 1000000 && ev_ps_ / 2 != 0)
                        {
                            ev_ps_ = ev_ps_ / 2;
                        }
                        active_box.setPosition(slowdown_button.getPosition());
                        active_box.setSize(slowdown_button.getSize());
                        gui_window.draw(active_box);
                    }
                    else if (rt_b.is_pressed(localPosition))
                    {
                        if (ev_ps_ == 0)
                        {
                            ev_ps_ = 60;
                        }
                        else
                        {
                            ev_ps_ = 0;
                        }
                    }
                    else if (!w2_ok_ && sw_b.is_pressed(localPosition) && n_ev_ != 0)
                    {
                        if (new_window.joinable())
                        {
                            new_window.join();
                        }
                        active_box.setPosition(start_window_button.getPosition());
                        active_box.setSize(start_window_button.getSize());
                        gui_window.draw(active_box);
                        w2_ok_ = true;
                        new_window = std::thread([this] { this->draw_population_window(true); });
                    }
                }
                //if close was pressed closes window and stops simulation
                if (e.type == sf::Event::Closed)
                {
                    stop();
                    gui_window.close();
                }
            }
            //if simulation is not running closes this window
            if (!play_)
            {
                gui_window.close();
            }
            //if simulation is not paused highlights the play button
            if (!is_paused_)
            {
                active_box.setPosition(pause_button.getPosition());
                active_box.setSize(pause_button.getSize());
                gui_window.draw(active_box);
            }
            //if simulation is running realtime highlights realtime button
            if (ev_ps_ == 0)
            {
                active_box.setPosition(realtime_button.getPosition());
                active_box.setSize(realtime_button.getSize());
                gui_window.draw(active_box);
            }
            //if population window is closed draws start population window button
            if (!w2_ok_ && n_ev_ != 0)
            {
                gui_window.draw(start_window_button);
            }

            counters_mutex_.lock();
            S.setString("S = " + std::to_string(S_));
            I.setString("I = " + std::to_string(I_));
            R.setString("R = " + std::to_string(R_));
            c_D.setString("D = " + std::to_string(D_));
            t_e.setString("Elapsed Time = " + std::to_string(elapsed_time_));
            evo.setString("Computed Evolutions = " + std::to_string(n_ev_));
            counters_mutex_.unlock();
            //draws remaining objects
            gui_window.draw(stat_background);
            gui_window.draw(pause_button);
            gui_window.draw(speedup_button);
            gui_window.draw(slowdown_button);
            gui_window.draw(realtime_button);
            gui_window.draw(S);
            gui_window.draw(I);
            gui_window.draw(R);
            gui_window.draw(c_D);
            gui_window.draw(t_e);
            gui_window.draw(evo);
            gui_window.display();
            w3_ok_ = true;
        }
        if (is_paused_)
            {
                pause_mutex_.unlock();
            }
        //rendering is done, w3_ok_ should be false
        w3_ok_ = false;
    }

    /**
     * @brief initializes and renders population window
     * 
     * @param display if false does nothing
     */
    void society::draw_population_window(bool display)
    {
        if (display)
        {
            windows_mutex_.lock();
            float h = sf::VideoMode::getDesktopMode().height;
            float w = sf::VideoMode::getDesktopMode().width;

            sf::ContextSettings pop_settings;
            pop_settings.antialiasingLevel = 4;

            float D;
            if (w < h)
                D = l_ / (w * 9 / 10);
            else
                D = l_ / (h * 9 / 10);

            sf::Vector2i w_pos(0, 0);

            sf::RenderWindow population_window(sf::VideoMode(l_ / D, l_ / D), "population", sf::Style::Default, pop_settings); //population render window
            population_window.setActive(true);
            population_window.setPosition(w_pos);
            population_window.setVerticalSyncEnabled(false);

            sf::Image icon;
            if (icon.loadFromFile("resources/w_icon.png"))
                population_window.setIcon(128, 128, icon.getPixelsPtr());
            else
                std::cerr << "Could not load windows icon" << std::endl;

            sf::View population_view(sf::FloatRect(0, 0, l_ / D, l_ / D));
            float individual_radius = 0.1 * r_c_ / D;

            sf::CircleShape S_individual; //this shape represents a susceptible individual
            S_individual.setRadius(individual_radius);
            S_individual.setOrigin(S_individual.getRadius(), S_individual.getRadius());
            S_individual.setFillColor(sf::Color(255, 255, 255));

            sf::CircleShape I_individual; //this shape represents an infectious individual
            I_individual.setRadius(individual_radius);
            I_individual.setOrigin(I_individual.getRadius(), I_individual.getRadius());
            I_individual.setFillColor(sf::Color(255, 0, 0));

            sf::CircleShape I_area; //this shape represents the area of possible infection
            I_area.setRadius(r_c_ / D);
            I_area.setOrigin(r_c_ / D, r_c_ / D);
            I_area.setFillColor(sf::Color(255, 0, 0, 64));

            sf::CircleShape R_individual; //this shape represents a recovered individual
            R_individual.setRadius(individual_radius);
            R_individual.setOrigin(R_individual.getRadius(), R_individual.getRadius());
            R_individual.setFillColor(sf::Color(0, 255, 0));

            sf::CircleShape work_border; //this shape represents the border of a working place
            work_border.setRadius(2.0F * r_c_ / D - individual_radius);
            work_border.setOrigin(work_border.getRadius(), work_border.getRadius());
            work_border.setFillColor(sf::Color::Transparent);
            work_border.setOutlineThickness(-individual_radius);
            int i = 0;
            work_border.setOutlineColor(sf::Color(0, 0, 255, i));

            sf::RectangleShape population_border; //rectangle representing simulation borders
            population_border.setPosition(0, 0);
            population_border.setSize(sf::Vector2f(l_ / D, l_ / D));
            population_border.setFillColor(sf::Color::Transparent);
            population_border.setOutlineThickness(I_area.getRadius());
            population_border.setOutlineColor(sf::Color::White);

            sf::Event e;               //event recorded in this window
            sf::Clock nav_clock;       //clock used to keep constant the movement of the view
            sf::Clock animation_clock; //clock used to draw work buildings border
            population_window.requestFocus();
            windows_mutex_.unlock();
            while (population_window.isOpen())
            {
                population_window.clear(sf::Color::Black);
                while (population_window.pollEvent(e))
                {
                    //resizing view when mousewheel is scrolled
                    if (e.type == sf::Event::MouseWheelScrolled)
                    {
                        if (e.mouseWheelScroll.delta < 0 && (population_view.getSize().x * (1 - e.mouseWheelScroll.delta / 10.) <= 2. * l_ / D))
                        {
                            population_view.setSize(population_view.getSize().x * (1 - e.mouseWheelScroll.delta / 10.), population_view.getSize().y * (1 - e.mouseWheelScroll.delta / 10.));
                        }
                        else if (e.mouseWheelScroll.delta > 0 && (population_view.getSize().x / (1 + e.mouseWheelScroll.delta / 10.) >= 10 * S_individual.getRadius()))
                        {
                            population_view.setSize(population_view.getSize().x / (1 + e.mouseWheelScroll.delta / 10.), population_view.getSize().y / (1 + e.mouseWheelScroll.delta / 10.));
                        }
                    }
                    //if window is resized change view
                    else if (e.type == sf::Event::Resized)
                    {
                        // update the view to the new size of the window
                        population_view.setSize(e.size.width, e.size.height);
                    }
                    else if (e.type == sf::Event::Closed)
                    {
                        population_window.close();
                    }
                }
                //if window has focus manage view movement through arrow keys
                if (population_window.hasFocus() && (nav_clock.getElapsedTime().asMilliseconds() > 5))
                {
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && ((population_view.getCenter().y - (population_view.getSize().y / 2.) - population_view.getSize().y / 200.) >= -0.5 * l_ / D))
                    {
                        population_view.move(sf::Vector2f(0, -population_view.getSize().y / 200.));
                    }
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && ((population_view.getCenter().y + (population_view.getSize().y / 2.) + population_view.getSize().y / 200.) <= 1.5 * l_ / D))
                    {
                        population_view.move(sf::Vector2f(0, population_view.getSize().y / 200.));
                    }
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && ((population_view.getCenter().x - (population_view.getSize().x / 2.) - population_view.getSize().x / 200.) >= -0.5 * l_ / D))
                    {
                        population_view.move(sf::Vector2f(-population_view.getSize().x / 200., 0));
                    }
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && ((population_view.getCenter().x + (population_view.getSize().x / 2.) + population_view.getSize().x / 200.) <= 1.5 * l_ / D))
                    {
                        population_view.move(sf::Vector2f(population_view.getSize().x / 200., 0));
                    }
                    nav_clock.restart();
                }
                if (!play_)
                {
                    population_window.close();
                }

                //if current day phase is day, draw work places
                if (phase_ == day_phase::day)
                {
                    if (i != 255 && animation_clock.getElapsedTime().asMilliseconds() > 5)
                    {
                        work_border.setOutlineColor(sf::Color(0, 0, 255, i += 5));
                        animation_clock.restart();
                    }
                    for (building const &b : work_buildings_)
                    {
                        work_border.setPosition(b.position.x() / D, b.position.y() / D);
                        population_window.draw(work_border);
                    }
                }
                //if current day phase is evening fade to black work places
                else if (phase_ == day_phase::evening)
                {
                    if (i != 0 && animation_clock.getElapsedTime().asMilliseconds() > 5)
                    {
                        work_border.setOutlineColor(sf::Color(0, 0, 255, i -= 5));
                        animation_clock.restart();
                    }
                    if (i != 0)
                    {
                        for (building const &b : work_buildings_)
                        {
                            work_border.setPosition(b.position.x() / D, b.position.y() / D);
                            population_window.draw(work_border);
                        }
                    }
                }
                else
                {
                    i = 0;
                }

                {
                    //copying population vector
                    people_tmp_mutex_.lock();
                    std::vector<individual> displayed_population = people_tmp_;
                    people_tmp_mutex_.unlock();
                    //draw individuals
                    for (auto &individual : displayed_population)
                    {
                        if (individual.sir.C == condition::I)
                        {
                            I_individual.setPosition((individual.position.x()) / D, (individual.position.y()) / D);
                            I_area.setPosition((individual.position.x()) / D, (individual.position.y()) / D);

                            population_window.draw(I_individual);
                            if (individual.sir.T_I > inc_t_)
                            {
                                population_window.draw(I_area);
                            }
                        }
                        else if (individual.sir.C == condition::S)
                        {
                            S_individual.setPosition((individual.position.x()) / D, (individual.position.y()) / D);
                            population_window.draw(S_individual);
                        }
                        else
                        {
                            R_individual.setPosition((individual.position.x()) / D, (individual.position.y()) / D);
                            population_window.draw(R_individual);
                        }
                    }
                }

                population_window.draw(population_border);
                population_window.setView(population_view);
                population_window.display();
                w2_ok_ = true;
            }
            w2_ok_ = false;
        }
    }
} // namespace sir
