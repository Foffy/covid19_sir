#include "population.hpp"
#include <iostream>

int main()
{
    sir::society sample(20000, 1, 0.01f, 0.01f);
    if(sample.generate_sample())
    {
        std::cout << "Times sample successfully generated, infection times have been written to \"Times_Sample.txt\"" << '\n';
    }
    else
    {
        std::cout << "Sample generation incomplete, generated times have been written to \"Times_Sample.txt\"" << '\n';
    }
    
}