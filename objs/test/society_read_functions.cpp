#include "population.hpp"

namespace sir
{
    bool operator==(society const &s1, society const &s2) 
    {
        if (s1.population_size_ == s2.population_size_ && s1.r_c_ == s2.r_c_ && s1.P_rem_ == s2.P_rem_ && s1.p_ == s2.p_ && s1.min_inf_t_ == s2.min_inf_t_)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    bool operator!=(society const &s1, society const &s2)
    {
        if (s1.population_size_ == s2.population_size_ && s1.r_c_ == s2.r_c_ && s1.P_rem_ == s2.P_rem_ && s1.p_ == s2.p_ && s1.min_inf_t_ == s2.min_inf_t_)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    int society::pop_size() const
    {
        return S_ + I_ + R_;
    }

    int const &society::S() const
    {
        return S_;
    }

    int const &society::I() const
    {
        return I_;
    }

    int const &society::R() const
    {
        return R_;
    }
    std::vector<building> const &society::home_buildings() const
    {
        return home_buildings_;
    }

    std::vector<building> const &society::work_buildings() const
    {
        return work_buildings_;
    }
} // namespace sir