#include "doctest.h"
#include "population.hpp"
#include "structures.hpp"
#include <vector>
#include <thread>
#include <fstream>
#include <algorithm>

/**
 * @brief checks Vector2D operators behave as expected
 * 
 */
TEST_CASE("VECTORS")
{
    CHECK(true); //testing doctest

    //initializing test objects
    sir::Vector2D<double> p{2., 1.};
    sir::Vector2D<double> const p1{3., 5.};
    //testing Vector2D operators
    CHECK(p == p);
    CHECK(p != p1);
    CHECK(p + p1 == sir::Vector2D{5., 6.});
    CHECK(p - p1 == sir::Vector2D{-1., -4.});
    CHECK(p1 - p == sir::Vector2D{1., 4.});
    CHECK(2 * p == sir::Vector2D{4., 2.});
    CHECK(norm(p) == doctest::Approx(2.23).epsilon(0.01));
    CHECK(dot_product(p, p1) == 11.);
    p += p1;
    CHECK(p == sir::Vector2D{5., 6.});
}

/**
 * @brief checks operators == and != behave as expected
 * 
 */
TEST_CASE("SOCIETY")
{
    CHECK(true); //testing doctest

    sir::epidemic_parameters par;
    //initializing test objects
    sir::society test_soc(par);
    sir::society test_soc_1(par);
    par.N = 10;
    sir::society test_soc_2(par);
    std::vector<sir::building> const &b = test_soc_2.home_buildings();
    for (int i = 0; i != int(b.size()); ++i)
    {
        bool right_distanced = std::none_of(b.begin(), b.begin() + i, [&](sir::building const& n) -> bool { return norm(b[i].position - n.position) < 2 * par.r_c; }) && std::none_of(b.begin() + i + 1, b.end(), [&](sir::building n) -> bool { return norm(b[i].position - n.position) < 2 * par.r_c; });
        CHECK(right_distanced);
    }
    std::vector<sir::building> const &w = test_soc_2.work_buildings();
    for (int i = 0; i != int(b.size()); ++i)
    {
        bool right_distanced = std::none_of(w.begin(), w.begin() + i, [&](sir::building const& n) -> bool { return norm(b[i].position - n.position) < 2 * par.r_c; }) && std::none_of(b.begin() + i + 1, b.end(), [&](sir::building n) -> bool { return norm(b[i].position - n.position) < 2 * par.r_c; });
        CHECK(right_distanced);
    }

    for (int i = 0; i != int(w.size()); ++i)
    {
        bool right_distanced = std::none_of(b.begin(), b.begin() + i, [&](sir::building const& n) -> bool { return norm(w[i].position - n.position) < 2 * par.r_c; }) && std::none_of(b.begin() + i + 1, b.end(), [&](sir::building n) -> bool { return norm(b[i].position - n.position) < 2 * par.r_c; });
        CHECK(right_distanced);
    }

    //testing operators == and !=, objects initialized with the same variables should be equal
    CHECK(test_soc == test_soc_1);
    CHECK(test_soc == test_soc);
    CHECK(test_soc_1 == test_soc_1);
    CHECK(test_soc_2 == test_soc_2);
    CHECK(test_soc != test_soc_2);
    CHECK(test_soc_1 != test_soc_2);

    sir::epidemic_parameters par1;
    par1.N = -200;
    CHECK_THROWS(sir::society test_1(par1));
    par1.N = 0.9;
    CHECK_THROWS(sir::society test_1(par1));
    par1.N = 200;
    par1.l = -300;
    CHECK_THROWS(sir::society test_1(par1));
    par1.l = 300;
    par1.r_c = -1;
    CHECK_THROWS(sir::society test_1(par1));
    par1.r_c = 5;
    par1.p = 2.6;
    CHECK_THROWS(sir::society test_1(par1));
    par1.p = 0.1;
    par1.N = 10;
    par1.l = 10;
    CHECK_THROWS(sir::society test_1(par1));
    par1.N = 1;
    par1.l = 10;
    CHECK_THROWS(sir::society test_1(par1));
}
/**
 * @brief checks evolve method behaves as expected
 * 
 */
TEST_CASE("EVOLUTION")
{
    CHECK(true); //testing doctest
    sir::epidemic_parameters par;
    par.min_inf_t = 0L;
    par.l = 1000.;
    par.N = 1;
    par.p_rem = 1L;
    sir::society test_soc(par);

    //test_soc.stop() should return false because population is not evolving
    CHECK(!test_soc.stop());
    std::thread test(&sir::society::single_evolve, &test_soc, par);
    test.join();
    CHECK(!test_soc.stop());
    sir::society test_soc_1(par);
    CHECK(test_soc_1.multi_evolve(par));
    CHECK(test_soc_1.I() == 0);
    CHECK(test_soc_1.S() + test_soc_1.R() == test_soc_1.pop_size());
    CHECK(!test_soc_1.stop());
    std::ifstream file;

    long double time = 0.; //current read time value
    double s = 0.;         //susceptibles
    double i = 0.;         //infects
    double r = 0.;         //removed
    double d = 0.;         //deaths
    double n = 0.;         //total
    double c_i = 0.;       //cumulative infections

    //checking the only infect individual recovers on elapsed_time_ = dt_ (p_rem = 1)
    //there should be only 2 printed times
    file.open("Data.txt");
    while (file.good())
    {
        file >> time >> s >> i >> r >> d >> n >> c_i;
        //if should be executed only one time (time = dt)
        if (time > 0.L)
        {
            CHECK(s == 0.);
            CHECK(i == 0.);
            CHECK(((r == 1.) || (d == 1.)));
            CHECK((n == s + i + r));
            CHECK(c_i == 1.);
        }
        //else should be executed only one time (time = 0)
        else
        {
            CHECK(s == 0.);
            CHECK(i == 1.);
            CHECK(r == 0.);
            CHECK(n == 1.);
            CHECK(c_i == 1.);
        }
    }
    file.close();
}