#include "structures.hpp"
#include "population.hpp"
#include <iostream>
/**
 * @brief takes in input simulation parameters
 * 
 * @return int (0 = success)
 */
int main()
{
  while (true)
  {
    bool retry = false;           //if true, cycle is repeated
    sir::epidemic_parameters par; //object used to initialize sir::society
    par.configure();
    if (par.init_window())
    {
      if (!par.init_parameters()) //call method init_parameters to initialize simulation parameters, if initialization was unsuccessfull abort
      {
        std::cout << "Aborting: parameters initialization was aborted" << std::endl;
        return 1;
      }
    }
    try
    {
      //while (true)
      //{
      sir::society p(par);

      if (p.single_evolve(par)) //call evolution method
      {
        std::cout << "Epidemic Ended" << '\n';
      }
      else
      {
        std::cout << "Program was aborted, evolution not completed" << '\n';
      }
      //}
    }
    catch (sir::simulation_error &e)
    {
      std::cerr << e.what() << std::endl;
      if (e.try_recover()) //if try_recover is successful, retry is set to true and cycle doesn't break
      {
        retry = true;
      }
      else
      {
        std::cerr << "Aborting ..." << std::endl;
        retry = false;
        return 1;
      }
    }
    catch (std::bad_alloc &e)
    {
      std::cerr << "Error:  " << e.what() << '\n'
                << "Population Size is too big, could not allocate memory, aborting ..." << std::endl;
      return 1;
    }
    catch (...)
    {
      std::cerr << "Uknown Generic Error, aborting ..." << std::endl;
      return 1;
    }
    if (!retry)
    {
      break;
    }
  }

  std::cout << '\n'
            << "Graph data has been written to Data.txt in the following order:" << '\n';
  std::cout << '\n'
            << "TIME    SUSCEPTIBLES    INFECTIOUS    RECOVERED     POPULATION      INFECTIONS" << '\n';
  std::cout << '\n'
            << "-------------------------------- PROGRAM ENDED --------------------------------" << '\n';
}