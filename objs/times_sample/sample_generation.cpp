#include "population.hpp"
#include <thread>
#include <chrono>

namespace sir {
    /**
     * @brief this function generates a sample of infection times
     * after the sample_gen constructor has been called to initialize a
     * sir::society object
     * @return true - Times sample has been fully generated
     * @return false - Generation was interrupted
     */
    bool society::generate_sample()
    {
        if (mode != compute_mode::sample_gen)
        {
            sir::simulation_error error(false);
            error.what("Current object wasn't properly constructed to generate a sample of times");
            throw error;
        }

        play_ = true;
        pause_mutex_.lock();
        //initializing a thread with display_windows method
        std::thread window([this] {this->display_windows(true, true);});

        bool not_done = true;
        output_file_.open("Times_Sample.txt");
        output_file_ << std::fixed;
        while (not_done && play_)
        {
            //checking if the user paused the evolution
            while (is_paused_ && play_)
            {
                pause_mutex_.lock();
                pause_mutex_.unlock();
            }

            //limiting max evolutions p.s.
            if (ev_ps_ > 1000000)
            {
                ev_ps_ = 0;
            }
            if (ev_ps_ != 0)
            {
                std::this_thread::sleep_for(std::chrono::microseconds(1000000 / ev_ps_));
            }

            //updating S.I.R. counters
            int S = 0;
            int I = 0;
            int R = 0;
            for (int i = 0; i != population_size_;++i) {
                if (people_[i].sir.C == condition::S) {
                    ++S;
                }
                else if (people_[i].sir.C == condition::I) {
                    ++I;
                }
                else {
                    ++R;
                }
            }
            S_ = S;
            I_ = I;
            R_ = R;

            //checking there are still people to infect
            if (I_ == population_size_)
            {
                not_done = false;
            }
            //exclude rendering of population
            people_tmp_mutex_.lock();
            people_tmp_ = people_;
            people_tmp_mutex_.unlock();

            //exclude rendering of counters
            counters_mutex_.lock();
            ++n_ev_;
            elapsed_time_ = n_ev_ * dt_;
            counters_mutex_.unlock();

            //infecting individuals
            for (int i = 0; i != population_size_; ++i)
            {
                SIR const &tmp = people_tmp_[i].sir;
                if (tmp.C == condition::S)
                {
                    float P = 1 - (pow((1 - p_), (tmp.contacts * dt_))); 

                    if (distr_(gen_) <= P)
                    {
                        output_file_ << elapsed_time_ << '\n';
                        people_[i].sir.C = condition::I;
                    }
                }
            }
        }
        output_file_ << std::flush;
        output_file_.close();
        window.join();

        if (I_ == population_size_)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}