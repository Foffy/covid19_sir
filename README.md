# SIR-based simulation model
## *UPDATES*
- Added simulation of deaths (death rate %)
- Added root macros executables to CMakeLists
- Added real-time graphing capabilities
- Performance improvements
## GOAL
The main goal of this project is to build from the ground up a simulator of an epidemic based on the Susceptible Infectious Removed model. Documentation is still only available in italian (Doc/).
## SOFTWARE REQUIREMENTS
Building the software requires a c++ compiler that supports the c++17 standard, a recent version of SFML and BOOST libraries, CMAKE 3.13.5 or higher. Data-analysis is done through the c++ framework ROOT using the included macros (*.cxx).
### GETTING DEPENDENCIES
Here's a handy list of commands to get all the needed dependencies for some package managers.
#### APT
```shell
$ apt-get install g++
$ apt-get install cmake
$ apt-get install libsfml-dev
$ apt-get install libboost-all-dev
```
#### PACMAN
```shell
$ pacman -S gcc
$ pacman -S cmake
$ pacman -S sfml
$ pacman -S boost
```
### HOW TO BUILD
Once software requirements are met we are ready to build:

```shell
$ cd /Path/To/Build_Folder
$ cmake /Path/To/Source_Folder
$ make -j10
```
### GETTING ROOT (DATA ANALYSIS)
"ROOT is a framework for data processing, born at CERN", there are [pre-built binaries](https://root.cern/releases/release-62202/) for many platforms, the software can also be built from the [source code](https://root.cern/download/root_v6.22.02.source.tar.gz). Here's the [official install guide](https://root.cern/install/).
### USING ROOT MACROS
Macros are written in c++, their extension (in this case) is "*.cxx". 

Here are two examples of macro execution, the first takes the "Covid-19_SIR" executable output the second the "Generate_Times_Sample" executable output.

(1)


```shell
$ cd /Path/To/Build_Folder
$ root
   ------------------------------------------------------------------
| Welcome to ROOT 6.22/00                        https://root.cern |
| (c) 1995-2020, The ROOT Team; conception: R. Brun, F. Rademakers |
| Built for linuxx8664gcc on Jun 14 2020, 15:54:05                 |
| From tags/v6-22-00@v6-22-00                                      |
| Try '.help', '.demo', '.license', '.credits', '.quit'/'.q'       |
------------------------------------------------------------------

root [0] .x simulation_graph.cxx
Info in <TCanvas::Print>: pdf file simulation.pdf has been created
Info in <TCanvas::Print>: file simulation.png has been created
root [1] 
```
![first_graph](Doc/simulation.png)
(2)


```shell
$ cd /Path/To/Build_Folder
$ root
   ------------------------------------------------------------------
| Welcome to ROOT 6.22/00                        https://root.cern |
| (c) 1995-2020, The ROOT Team; conception: R. Brun, F. Rademakers |
| Built for linuxx8664gcc on Jun 14 2020, 15:54:05                 |
| From tags/v6-22-00@v6-22-00                                      |
| Try '.help', '.demo', '.license', '.credits', '.quit'/'.q'       |
------------------------------------------------------------------

root [0] .x histogram.cxx
FCN=15.1669 FROM MIGRAD    STATUS=CONVERGED      17 CALLS          18 TOTAL
EDM=3.08106e-20    STRATEGY= 1      ERROR MATRIX ACCURATE 
EXT PARAMETER                                   STEP         FIRST   
NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
1  p0           1.00000e+00     fixed    
2  p1           1.00000e-02     fixed    
3  Constant     5.61970e+06   1.77725e+04   1.08102e+01  -1.39675e-14
*----------------------------------------------------------*
Occorrenze Totali: 100000
Media dell'istogramma: 100.1 +/- 0.316174
RMS dell'istogramma: 99.9826 +/- 0.223569
Info in <TCanvas::Print>: pdf file T_i.pdf has been created
Info in <TCanvas::Print>: file T_i.png has been created
root [1] 
```
![second_graph](Doc/T_i_example.png)

## EXAMPLES
Examples are available in Doc/covid19_sir.pdf at the end of the document, no english translation is available yet.


