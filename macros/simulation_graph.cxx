#include <iostream>
#include <fstream>
#include "TMath.h"
#include "TStyle.h"
#include "TH1F.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TFile.h"
#include "TMatrixD.h"
#include "TFitResult.h"
#include "TROOT.h"
#include "TGraphErrors.h"

using namespace std;

void simulation_graph()
{ //graph function

  //global style
  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(2210);
  gStyle->SetOptFit(111);
  gStyle->SetStatY(0.99);
  gStyle->SetStatX(1);
  gStyle->SetStatW(0.1);
  gStyle->SetStatH(0.1);

  //building canvas and setting margins

  Double_t w = 3000;
  Double_t h = 2000;
  TCanvas *graph_canvas = new TCanvas("SIR simulation", "");
  graph_canvas->SetRightMargin(0.18);
  graph_canvas->SetLeftMargin(0.08);
  graph_canvas->SetBottomMargin(0.1);
  graph_canvas->SetTopMargin(0.05);
  graph_canvas->SetWindowSize(w + (w - graph_canvas->GetWw()), h + (h - graph_canvas->GetWh()));

  //building a Graph objects

  TGraphErrors *Susceptibles = new TGraphErrors("Data.txt", "%lg %lg");
  TGraphErrors *Infectious = new TGraphErrors("Data.txt", "%lg %*lg %lg");
  TGraphErrors *Recovered = new TGraphErrors("Data.txt", "%lg %*lg %*lg %lg");
  TGraphErrors *Deaths = new TGraphErrors("Data.txt", "%lg %*lg %*lg %*lg %lg");
  TGraphErrors *Population = new TGraphErrors("Data.txt", "%lg %*lg %*lg %*lg %*lg %lg");
  TGraphErrors *Infections = new TGraphErrors("Data.txt", "%lg %*lg %*lg %*lg %*lg %*lg %lg");

  ifstream data;
  data.open("Data.txt");
  float population;
  data >> population >> population >> population >> population >> population >> population;
  data.close();

  Infectious->SetMaximum(population + population / 10.);
  Infectious->SetTitle("");

  Susceptibles->SetMarkerColor(kBlack);
  Infectious->SetMarkerColor(kRed);
  Recovered->SetMarkerColor(kGreen);
  Infections->SetMarkerColor(kYellow);
  Population->SetMarkerColor(kBlue);
  Deaths->SetMarkerColor(6);

  Susceptibles->SetMarkerStyle(kFullCircle);
  Infectious->SetMarkerStyle(kFullCircle);
  Recovered->SetMarkerStyle(kFullCircle);
  Infections->SetMarkerStyle(kFullCircle);
  Population->SetMarkerStyle(kFullCircle);
  Deaths->SetMarkerStyle(kFullCircle);

  Susceptibles->SetMarkerSize(0.5);
  Infectious->SetMarkerSize(0.5);
  Recovered->SetMarkerSize(0.5);
  Infections->SetMarkerSize(0.5);
  Population->SetMarkerSize(0.5);
  Deaths->SetMarkerSize(0.5);

  Susceptibles->SetLineColor(kBlack);
  Infectious->SetLineColor(kRed);
  Recovered->SetLineColor(kGreen);
  Infections->SetLineColor(kYellow);
  Population->SetLineColor(kBlue);
  Deaths->SetLineColor(6);

  Infectious->SetLineWidth(2);
  Susceptibles->SetLineWidth(2);
  Recovered->SetLineWidth(2);
  Deaths->SetLineWidth(2);
  Infections->SetLineWidth(2);
  Population->SetLineWidth(2);

  //Setting axes titles

  Infectious->GetXaxis()->SetTitle("Time (u.t.)");
  Infectious->GetYaxis()->SetTitle("N (individuals)");
  Infectious->GetYaxis()->SetTitleOffset(1.1);

  //Drawing graph

  Infectious->Draw("AP");
  Susceptibles->Draw("P");
  Recovered->Draw("P");
  Infections->Draw("P");
  Population->Draw("P");
  Deaths->Draw("P");

  //building legend

  TLegend *leg = new TLegend(1., 0.95, 0.82, 0.79, "");
  leg->SetFillColor(0);
  leg->AddEntry(Susceptibles, "S");
  leg->AddEntry(Infectious, "I");
  leg->AddEntry(Recovered, "R");
  leg->AddEntry(Deaths, "D");
  leg->AddEntry(Population, "Total Population");
  leg->AddEntry(Infections, "Cumulative Infections");
  leg->Draw("Same");

  //outputting on files

  graph_canvas->Print("simulation.pdf");
  graph_canvas->Print("simulation.png");
}

int main(){
simulation_graph();
}
