#include <iostream>
#include <fstream>
#include "TMath.h"
#include "TStyle.h"
#include "TH1F.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TFile.h"
#include "TMatrixD.h"
#include "TFitResult.h"
#include "TROOT.h"

using namespace std;

void histogram()
{ //histogram function
   gStyle->SetOptTitle(0);
   gStyle->SetOptStat(2210);
   gStyle->SetOptFit(1110);
   gStyle->SetStatY(0.99);
   gStyle->SetStatX(0.97);
   gStyle->SetStatW(0.1);
   gStyle->SetStatH(0.1);

   ifstream find_max;
   find_max.open("Times_Sample.txt");
   Float_t max_time = 0;
   unsigned sample_size = 0;

   while (find_max.good())
   {
      find_max >> max_time;
      ++sample_size;
   }
   find_max.close();

   Float_t nbins = sample_size * (15. / 10000.);
   //building histogram

   TH1F *h1 = new TH1F("", "", nbins, 0, max_time + ((max_time) / (2 * nbins)));

   //filling histogram from file

   ifstream in;
   in.open("Times_Sample.txt");
   Float_t x;
   Int_t cont;
   //fill loop
   while (1)
   {
      in >> x;
      if (!in.good())
         break;
      h1->Fill(x);
   }

   TF1 *time_function = new TF1("Expected Times Distribution", "-[2]*[0]*log(1-[1])*pow((1-[1]),([0]*x))", 0, max_time); //function to fit to histogram
   time_function->FixParameter(0, 1);                                                                                    //contacts parameter
   time_function->FixParameter(1, 0.01);                                                                                 //probability parameter
   time_function->SetParName(0, "Contacts");
   time_function->SetParName(1, "Probability of Infection");
   time_function->SetParName(2, "Normalization");

   //Building canvas and setting margins

   Double_t w = 3000;
   Double_t h = 2000;
   TCanvas *canvas = new TCanvas("Fit of Time Function", "");
   canvas->SetLeftMargin(0.08);
   canvas->SetRightMargin(0.03);
   canvas->SetBottomMargin(0.12);
   canvas->SetTopMargin(0.01);
   canvas->SetWindowSize(w + (w - canvas->GetWw()), h + (h - canvas->GetWh()));

   //cosmetics

   h1->GetXaxis()->SetTitleOffset(1.3);
   h1->GetXaxis()->SetTitle("Time Before Infection #left(u.t.#right)");
   h1->GetYaxis()->SetTitleOffset(1.2);
   h1->GetYaxis()->SetTitle("Occurrences");

   h1->SetFillColor(17);
   h1->SetMarkerStyle(4);
   h1->SetMarkerColor(1);
   h1->SetLineWidth(4);
   h1->SetLineColor(1);

   h1->Fit(time_function);

   //Drawing histogram

   h1->Draw("E,same");
   TLegend *leg = new TLegend(0.97, 0.6, 0.79, 0.79, "");
   leg->SetFillColor(0);
   leg->AddEntry(h1, "Times Histogram");
   leg->AddEntry(time_function, "Expected Distribution");

   leg->Draw("Same");

   //outputting statistical results

   cout << "*----------------------------------------------------------*" << endl;

   cout << "Occorrenze Totali: " << h1->GetEntries() << endl;
   cout << "Media dell'istogramma: " << h1->GetMean() << " +/- " << h1->GetMeanError() << endl;
   cout << "RMS dell'istogramma: " << h1->GetRMS() << " +/- " << h1->GetRMSError() << endl;

   //output files

   canvas->Print("T_i.pdf");
   canvas->Print("T_i.png");
}
int main()
{
    histogram();
}
